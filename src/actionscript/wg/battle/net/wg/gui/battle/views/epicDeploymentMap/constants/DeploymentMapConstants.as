package net.wg.gui.battle.views.epicDeploymentMap.constants
{
    public class DeploymentMapConstants extends Object
    {

        public static const SCORE_PANEL_TOP_OFFSET:int = 108;

        public static const RESPAWN_ELEMENTS_SIZE:int = 305;

        public static const BORDERMAP_TO_MAP_RATIO:Number = 0.75;

        public static const BORDER_WIDTH_PERCENTAGE:Number = 0.125;

        public function DeploymentMapConstants()
        {
            super();
        }
    }
}
