package net.wg.gui.battle.views.minimap.components.entries.constants
{
    public class BackgroundMinimapEntryConst extends Object
    {

        public static const TURORIAL_ATLAS_ITEM_NAME:String = "TutorialEntry";

        public static const BOOTCAMP_ATLAS_ITEM_NAME:String = "BootcampEntry";

        public function BackgroundMinimapEntryConst()
        {
            super();
        }
    }
}
