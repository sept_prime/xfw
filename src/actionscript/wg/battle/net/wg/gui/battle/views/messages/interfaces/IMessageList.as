package net.wg.gui.battle.views.messages.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import net.wg.infrastructure.interfaces.entity.IDisplayableComponent;

    public interface IMessageList extends IDisposable, IDisplayableComponent
    {
    }
}
