package net.wg.gui.battle.random.views.stats.components.playersPanel.VO
{
    public class PlayersPanelContextMenuSentData extends Object
    {

        public var vehicleID:Number = NaN;

        public function PlayersPanelContextMenuSentData(param1:Number)
        {
            super();
            this.vehicleID = param1;
        }
    }
}
