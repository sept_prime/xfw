package net.wg.gui.bootcamp.interfaces
{
    import net.wg.infrastructure.base.meta.IBCBattleResultTransitionMeta;
    import net.wg.infrastructure.interfaces.IRootAppMainContent;

    public interface IBCBattleResultTransition extends IBCBattleResultTransitionMeta, IRootAppMainContent
    {
    }
}
