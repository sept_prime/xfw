package net.wg.gui.components.hintPanel
{
    import net.wg.infrastructure.base.meta.impl.BattleHintPanelMeta;
    import net.wg.infrastructure.base.meta.IBattleHintPanelMeta;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import scaleform.clik.motion.Tween;
    import net.wg.gui.battle.events.BattleSoundEvent;
    import net.wg.data.constants.Values;
    import flash.events.Event;
    import flash.display.BlendMode;
    import net.wg.data.constants.generated.BATTLE_SOUND;
    import fl.motion.easing.Quartic;

    public class HintPanel extends BattleHintPanelMeta implements IBattleHintPanelMeta
    {

        private static const TEXTFIELD_PADDING:int = 5;

        private static const GAP:int = 18;

        private static const BACKGROUND_INNER_PADDING:int = 48;

        private static const BG_INIT_Y:int = 48;

        private static const BG_INIT_SCALE:int = 0;

        private static const BG_INIT_ALPHA:int = 0;

        private static const BG_FADE_OUT_ALPHA:int = 1;

        private static const FADE_IN_FRAME:String = "fadeIn";

        private static const FADE_OUT_FRAME:String = "fadeOut";

        private static const FADE_IN_BG_DURATION:int = 200;

        private static const FADE_IN_BG_DELAY:int = 400;

        private static const FADE_IN_BG_SCALE_Y:int = 5;

        private static const FADE_IN_BG_ALPHA:int = 1;

        private static const FADE_OUT_BG_DURATION:int = 870;

        private static const FADE_OUT_BG_Y:int = 148;

        private static const FADE_OUT_BG_ALPHA:int = 0;

        private static const KEY_EFFECT_OFFSET_X:int = 3;

        public var appearanceEffectAnim:MovieClip = null;

        public var keyEffectAnim:KeyViewerContainerAnim = null;

        public var keyAnim:KeyViewerContainerAnim = null;

        public var messageLeftAnim:MessageAnim = null;

        public var messageRightAnim:MessageAnim = null;

        public var bg:Sprite = null;

        private var _fadeInBgTween:Tween = null;

        private var _fadeOutBgTween:Tween = null;

        private var _keyValue:String = "";

        private var _messageLeft:String = "";

        private var _messageRight:String = "";

        private var _offsetX:int = 0;

        private var _offsetY:int = 0;

        private var _keySelected:Boolean = false;

        private var _width:int = 0;

        private var _height:int = 0;

        public function HintPanel()
        {
            super();
            super.visible = false;
            this._height = this.bg.height / this.bg.scaleY * FADE_IN_BG_SCALE_Y;
            this.keyAnim.addFrameScript(this.keyAnim.totalFrames - 1,this.onFadeOutComplete);
            this._fadeInBgTween = new Tween(FADE_IN_BG_DURATION,this.bg,{
                "scaleY":FADE_IN_BG_SCALE_Y,
                "alpha":FADE_IN_BG_ALPHA
            },{
                "paused":true,
                "ease":Quartic.easeIn,
                "delay":FADE_IN_BG_DELAY,
                "onComplete":null
            });
            this._fadeInBgTween.fastTransform = false;
            this._fadeOutBgTween = new Tween(FADE_OUT_BG_DURATION,this.bg,{
                "y":FADE_OUT_BG_Y,
                "alpha":FADE_OUT_BG_ALPHA
            },{
                "paused":true,
                "onComplete":null
            });
            this._fadeOutBgTween.fastTransform = false;
        }

        override protected function onPopulate() : void
        {
            super.onPopulate();
            this.keyEffectAnim.addEventListener(BattleSoundEvent.ON_SOUND_PLAY,this.onKeyEffectAnimOnSoundPlayHandler);
            this.messageLeftAnim.x = BACKGROUND_INNER_PADDING;
        }

        override protected function onDispose() : void
        {
            this._fadeInBgTween.dispose();
            this._fadeInBgTween = null;
            this._fadeOutBgTween.dispose();
            this._fadeOutBgTween = null;
            this.appearanceEffectAnim.stop();
            this.appearanceEffectAnim = null;
            this.keyEffectAnim.removeEventListener(BattleSoundEvent.ON_SOUND_PLAY,this.onKeyEffectAnimOnSoundPlayHandler);
            this.keyEffectAnim.dispose();
            this.keyEffectAnim = null;
            this.keyAnim.addFrameScript(this.keyAnim.totalFrames - 1,null);
            this.keyAnim.dispose();
            this.keyAnim = null;
            this.messageLeftAnim.dispose();
            this.messageLeftAnim = null;
            this.messageRightAnim.dispose();
            this.messageRightAnim = null;
            this.bg = null;
            super.onDispose();
        }

        public function as_setData(param1:String, param2:String, param3:String, param4:int, param5:int) : void
        {
            if(param1 != this._keyValue)
            {
                this._keyValue = param1;
                this._keySelected = Boolean(param1);
                if(this._keySelected)
                {
                    this.keyAnim.setKey(param1);
                    this.keyEffectAnim.setKey(param1);
                }
            }
            if(this._keySelected)
            {
                if(param2 != this._messageLeft)
                {
                    this._messageLeft = param2;
                    this.messageLeftAnim.setText(param2);
                    this.messageLeftAnim.setTextFieldWidth(this.messageLeftAnim.textFieldContainer.textField.textWidth + TEXTFIELD_PADDING);
                }
            }
            if(param3 != this._messageRight)
            {
                this._messageRight = param3;
                this.messageRightAnim.setText(param3);
                this.messageRightAnim.setTextFieldWidth(this.messageRightAnim.textFieldContainer.textField.textWidth + TEXTFIELD_PADDING);
            }
            this._offsetX = param4;
            this._offsetY = param5;
            this.update();
        }

        public function hide() : void
        {
            this.visible = false;
        }

        public function show(param1:String, param2:String, param3:String) : void
        {
            this.as_setData(param1,param2,param3,Values.ZERO,Values.ZERO);
        }

        private function update() : void
        {
            var _loc3_:* = 0;
            this.messageLeftAnim.visible = this._keySelected;
            this.keyEffectAnim.visible = this._keySelected;
            this.keyAnim.visible = this._keySelected;
            if(this._keySelected)
            {
                _loc3_ = (this.keyAnim.width >> 1) + BACKGROUND_INNER_PADDING + this.messageLeftAnim.width + GAP ^ 0;
                this.keyAnim.x = _loc3_;
                this.keyEffectAnim.x = _loc3_ - KEY_EFFECT_OFFSET_X;
                this.messageRightAnim.x = _loc3_ + this.keyAnim.width;
            }
            else
            {
                this.messageRightAnim.x = BACKGROUND_INNER_PADDING;
            }
            var _loc1_:* = this.messageRightAnim.x + this.messageRightAnim.width + BACKGROUND_INNER_PADDING ^ 0;
            if(_loc1_ != this._width)
            {
                this._width = _loc1_;
                dispatchEvent(new Event(Event.RESIZE));
            }
            var _loc2_:* = this._width >> 1;
            this.bg.width = this._width;
            this.bg.x = _loc2_;
            this.appearanceEffectAnim.x = _loc2_;
        }

        private function onFadeOutComplete() : void
        {
            super.visible = false;
        }

        private function playSnd(param1:String) : void
        {
            onPlaySoundS(param1);
        }

        override public function set visible(param1:Boolean) : void
        {
            if(super.visible != param1)
            {
                if(param1)
                {
                    super.visible = param1;
                    this._fadeOutBgTween.paused = true;
                    this.bg.y = BG_INIT_Y;
                    this.bg.scaleY = BG_INIT_SCALE;
                    this.bg.alpha = BG_INIT_ALPHA;
                    this.keyEffectAnim.gotoAndPlay(FADE_IN_FRAME);
                    this.keyAnim.gotoAndPlay(FADE_IN_FRAME);
                    this.keyAnim.blendMode = BlendMode.SCREEN;
                    this.messageLeftAnim.gotoAndPlay(FADE_IN_FRAME);
                    this.messageRightAnim.gotoAndPlay(FADE_IN_FRAME);
                    this.appearanceEffectAnim.gotoAndPlay(FADE_IN_FRAME);
                    this._fadeInBgTween.reset();
                    this._fadeInBgTween.paused = false;
                }
                else
                {
                    this._fadeInBgTween.paused = true;
                    this.bg.y = BG_INIT_Y;
                    this.bg.scaleY = FADE_IN_BG_SCALE_Y;
                    this.bg.alpha = BG_FADE_OUT_ALPHA;
                    this.keyEffectAnim.gotoAndStop(1);
                    this.keyAnim.gotoAndPlay(FADE_OUT_FRAME);
                    this.keyAnim.blendMode = BlendMode.ADD;
                    this.messageLeftAnim.gotoAndPlay(FADE_OUT_FRAME);
                    this.messageRightAnim.gotoAndPlay(FADE_OUT_FRAME);
                    this._fadeOutBgTween.reset();
                    this._fadeOutBgTween.paused = false;
                }
                this.playSnd(param1?BATTLE_SOUND.SOUND_TYPE_HINT_PANNEL_SHOW:BATTLE_SOUND.SOUND_TYPE_HINT_PANNEL_HIDE);
            }
        }

        override public function get width() : Number
        {
            return this._width;
        }

        override public function get height() : Number
        {
            return this._height;
        }

        public function get offsetX() : int
        {
            return this._offsetX;
        }

        public function get offsetY() : int
        {
            return this._offsetY;
        }

        private function onKeyEffectAnimOnSoundPlayHandler(param1:BattleSoundEvent) : void
        {
            this.playSnd(param1.soundType);
        }
    }
}
