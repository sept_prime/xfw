package net.wg.data.constants
{
    public class Fonts extends Object
    {

        public static const FIELD_FONT:String = "$FieldFont";

        public static const TEXT_FONT:String = "$TextFont";

        public static const TITLE_FONT:String = "$TitleFont";

        public function Fonts()
        {
            super();
        }
    }
}
