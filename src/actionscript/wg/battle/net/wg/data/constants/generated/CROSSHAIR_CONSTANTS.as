package net.wg.data.constants.generated
{
    public class CROSSHAIR_CONSTANTS extends Object
    {

        public static const INVISIBLE:int = 0;

        public static const VISIBLE_NET:int = 1;

        public static const VISIBLE_AMMO_COUNT:int = 2;

        public static const VISIBLE_ALL:int = 3;

        public function CROSSHAIR_CONSTANTS()
        {
            super();
        }
    }
}
