package net.wg.gui.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IUpdatable;
    import net.wg.infrastructure.interfaces.IUIComponentEx;

    public interface IUpdatableComponent extends IUpdatable, IUIComponentEx
    {
    }
}
