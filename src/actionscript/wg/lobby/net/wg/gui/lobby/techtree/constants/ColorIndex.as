package net.wg.gui.lobby.techtree.constants
{
    public class ColorIndex extends Object
    {

        public static const UNLOCKED:uint = 0;

        public static const LOCKED:uint = 1;

        public static const PREMIUM:uint = 2;

        public static const DEFAULT:uint = LOCKED;

        public function ColorIndex()
        {
            super();
        }
    }
}
