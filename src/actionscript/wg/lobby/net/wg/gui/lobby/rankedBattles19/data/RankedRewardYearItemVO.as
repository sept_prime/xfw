package net.wg.gui.lobby.rankedBattles19.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class RankedRewardYearItemVO extends DAAPIDataClass
    {

        public var id:String = "";

        public var status:String = "";

        public function RankedRewardYearItemVO(param1:Object = null)
        {
            super(param1);
        }
    }
}
