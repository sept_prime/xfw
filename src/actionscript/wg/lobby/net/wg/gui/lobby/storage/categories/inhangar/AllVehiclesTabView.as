package net.wg.gui.lobby.storage.categories.inhangar
{
    import net.wg.infrastructure.base.meta.impl.AllVehiclesTabViewMeta;
    import net.wg.infrastructure.base.meta.IAllVehiclesTabViewMeta;
    import net.wg.gui.lobby.storage.categories.NoItemsView;
    import scaleform.clik.interfaces.IDataProvider;
    import net.wg.data.VO.FilterDAAPIDataProvider;
    import net.wg.gui.lobby.storage.categories.cards.VehicleCardVO;
    import flash.events.Event;
    import net.wg.gui.lobby.storage.categories.cards.CardEvent;
    import net.wg.gui.events.FiltersEvent;
    import net.wg.data.constants.Linkages;
    import scaleform.clik.constants.InvalidationType;
    import scaleform.clik.core.UIComponent;

    public class AllVehiclesTabView extends AllVehiclesTabViewMeta implements IAllVehiclesTabViewMeta
    {

        private static const CAROUSEL_TOP_OFFSET:int = 20;

        private static const CAROUSEL_BOTTOM_OFFSET:int = 30;

        public var noItemsView:NoItemsView;

        public var filtersBlock:InHangarFilterBlock = null;

        public function AllVehiclesTabView()
        {
            super();
        }

        override protected function hasNoFilterWarningView() : Boolean
        {
            return true;
        }

        override protected function doPartlyVisibility(param1:Boolean, param2:Boolean) : void
        {
            super.doPartlyVisibility(param1,param2);
            this.filtersBlock.visible = !param1 || param2;
        }

        override protected function getNewCardDP() : IDataProvider
        {
            return new FilterDAAPIDataProvider(VehicleCardVO);
        }

        override protected function onDispose() : void
        {
            this.noItemsView.removeEventListener(Event.CLOSE,this.onNoItemViewCloseHandler);
            carousel.removeEventListener(CardEvent.SELL,this.onCardSellHandler);
            this.filtersBlock.removeEventListener(FiltersEvent.SEARCH_VALUE_CHANGED,this.onFiltersBlockSearchValueChangedHandler);
            this.filtersBlock.removeEventListener(FiltersEvent.RESET_ALL_FILTERS,this.onFiltersBlockResetAllFiltersHandler);
            this.filtersBlock.dispose();
            this.filtersBlock = null;
            this.noItemsView.dispose();
            this.noItemsView = null;
            super.onDispose();
        }

        override protected function configUI() : void
        {
            super.configUI();
            carousel.scrollList.itemRendererClassReference = Linkages.VEHICLE_CARD_RENDERER;
            carousel.scrollList.paddingTop = CAROUSEL_TOP_OFFSET;
            carousel.scrollList.paddingBottom = CAROUSEL_BOTTOM_OFFSET;
            carousel.addEventListener(CardEvent.SELL,this.onCardSellHandler);
            this.noItemsView.setTexts(STORAGE.INHANGAR_NOITEMS_ALLTAB_TITLE,STORAGE.STORAGE_NOITEMS_NAVIGATIONBUTTON);
            this.noItemsView.addEventListener(Event.CLOSE,this.onNoItemViewCloseHandler);
            this.filtersBlock.addEventListener(FiltersEvent.SEARCH_VALUE_CHANGED,this.onFiltersBlockSearchValueChangedHandler);
            this.filtersBlock.addEventListener(FiltersEvent.RESET_ALL_FILTERS,this.onFiltersBlockResetAllFiltersHandler);
        }

        override protected function draw() : void
        {
            super.draw();
            if(isInvalid(InvalidationType.SIZE))
            {
                this.filtersBlock.x = carousel.x;
                this.filtersBlock.width = carousel.width;
                this.filtersBlock.validateNow();
                this.noItemsView.width = width;
                this.noItemsView.validateNow();
                this.noItemsView.y = height - this.noItemsView.actualHeight >> 1;
            }
        }

        override protected function onDummyEvent(param1:String) : void
        {
            resetFilterS();
        }

        public function as_updateCounter(param1:Boolean, param2:String, param3:Boolean) : void
        {
            this.filtersBlock.updateCounter(param1,param2,param3);
        }

        public function as_updateSearch(param1:String, param2:String, param3:String, param4:int) : void
        {
            this.filtersBlock.updateSearch(param1,param2,param3,param4);
        }

        private function onCardSellHandler(param1:CardEvent) : void
        {
            param1.stopImmediatePropagation();
            sellItemS(param1.data.id);
        }

        private function onNoItemViewCloseHandler(param1:Event) : void
        {
            navigateToStoreS();
        }

        private function onFiltersBlockSearchValueChangedHandler(param1:FiltersEvent) : void
        {
            changeSearchNameVehicleS(param1.searchValue);
        }

        private function onFiltersBlockResetAllFiltersHandler(param1:FiltersEvent) : void
        {
            resetFilterS();
        }

        override public function get noItemsComponent() : UIComponent
        {
            return this.noItemsView;
        }
    }
}
