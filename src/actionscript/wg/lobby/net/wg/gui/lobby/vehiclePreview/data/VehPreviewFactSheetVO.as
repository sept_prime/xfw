package net.wg.gui.lobby.vehiclePreview.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class VehPreviewFactSheetVO extends DAAPIDataClass
    {

        public var historicReferenceTxt:String = "";

        public function VehPreviewFactSheetVO(param1:Object)
        {
            super(param1);
        }
    }
}
