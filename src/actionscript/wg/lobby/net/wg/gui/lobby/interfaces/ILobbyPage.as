package net.wg.gui.lobby.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IDraggable;
    import net.wg.infrastructure.base.meta.ILobbyPageMeta;

    public interface ILobbyPage extends IDraggable, ILobbyPageMeta
    {
    }
}
