package net.wg.gui.lobby.hangar.ammunitionPanel
{
    import net.wg.utils.helpLayout.IHelpLayoutComponent;
    import net.wg.infrastructure.base.meta.IAmmunitionPanelMeta;
    import net.wg.infrastructure.interfaces.entity.IFocusContainer;

    public interface IAmmunitionPanel extends IHelpLayoutComponent, IAmmunitionPanelMeta, IFocusContainer
    {
    }
}
