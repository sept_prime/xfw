package net.wg.gui.lobby.quests.components.interfaces
{
    import net.wg.infrastructure.interfaces.entity.IUpdatable;
    import net.wg.infrastructure.interfaces.entity.IDisposable;

    public interface ITextBlockWelcomeView extends IUpdatable, IDisposable
    {
    }
}
