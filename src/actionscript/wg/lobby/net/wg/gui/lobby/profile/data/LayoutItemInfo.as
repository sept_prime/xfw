package net.wg.gui.lobby.profile.data
{
    import flash.display.DisplayObject;

    public class LayoutItemInfo extends Object
    {

        public var item:DisplayObject;

        public var position:Number;

        public function LayoutItemInfo()
        {
            super();
        }
    }
}
