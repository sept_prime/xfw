package net.wg.gui.lobby.vehicleCustomization
{
    public class CustomizationHelper extends Object
    {

        public static const RENDER_STATE_BUY_MODE:String = "rendererStateBuyMode";

        public static const RENDER_STATE_BASE_MODE:String = "rendererStateBaseMode";

        public function CustomizationHelper()
        {
            super();
        }
    }
}
