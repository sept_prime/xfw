package net.wg.gui.lobby.linkedSet.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class LinkedSetDetailsVideoVO extends DAAPIDataClass
    {

        public var headerServiceText:String = "";

        public var headerDepotText:String = "";

        public var headerConfirmSaleText:String = "";

        public var headerEquipmentText:String = "";

        public var btnBattleText:String = "";

        public var btnAcceptText:String = "";

        public var btnResearchText:String = "";

        public var btnStoreText:String = "";

        public var btnSellText:String = "";

        public var btnCancelText:String = "";

        public var txtTotalText:String = "";

        public var headerPersonalReservesText:String = "";

        public var btnActivateText:String = "";

        public var txtMyProfileText:String = "";

        public var headerExteriorText:String = "";

        public var btnExteriorText:String = "";

        public var btnServiceText:String = "";

        public var btnPurchaseText:String = "";

        public function LinkedSetDetailsVideoVO(param1:Object)
        {
            super(param1);
        }
    }
}
