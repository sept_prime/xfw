package net.wg.gui.prebattle.base
{
    import net.wg.infrastructure.base.meta.impl.BasePrebattleListViewMeta;
    import net.wg.infrastructure.base.meta.IBasePrebattleListViewMeta;

    public class BasePrebattleListView extends BasePrebattleListViewMeta implements IBasePrebattleListViewMeta
    {

        public function BasePrebattleListView()
        {
            super();
        }

        public function as_getSearchDP() : Object
        {
            return null;
        }
    }
}
