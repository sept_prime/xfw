package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IStorageCategoryCustomizationViewMeta extends IEventDispatcher
    {

        function navigateToCustomizationS() : void;

        function sellItemS(param1:Number) : void;
    }
}
