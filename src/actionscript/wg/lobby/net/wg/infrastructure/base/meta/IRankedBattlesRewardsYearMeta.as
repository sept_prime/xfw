package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IRankedBattlesRewardsYearMeta extends IEventDispatcher
    {

        function as_setData(param1:Object) : void;
    }
}
