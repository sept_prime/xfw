package net.wg.data.constants.generated
{
    public class DAMAGE_LOG_SHELL_BG_TYPES extends Object
    {

        public static const GOLD:String = "gold";

        public static const WHITE:String = "white";

        public static const EMPTY:String = "empty";

        public function DAMAGE_LOG_SHELL_BG_TYPES()
        {
            super();
        }
    }
}
