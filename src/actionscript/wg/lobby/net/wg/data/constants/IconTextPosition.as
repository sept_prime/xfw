package net.wg.data.constants
{
    public class IconTextPosition extends Object
    {

        public static const LEFT:String = "left";

        public static const RIGHT:String = "right";

        public static const TOP:String = "top";

        public static const BOTTOM:String = "bottom";

        public function IconTextPosition()
        {
            super();
        }
    }
}
