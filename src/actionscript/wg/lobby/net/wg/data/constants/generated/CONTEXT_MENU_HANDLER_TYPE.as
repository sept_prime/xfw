package net.wg.data.constants.generated
{
    public class CONTEXT_MENU_HANDLER_TYPE extends Object
    {

        public static const CREW:String = "crew";

        public static const CHANNEL_LIST:String = "channelList";

        public static const VEHICLE:String = "vehicle";

        public static const RESEARCH_ITEM:String = "researchItem";

        public static const RESEARCH_VEHICLE:String = "researchVehicle";

        public static const BLUEPRINT_VEHICLE:String = "blueprintVehicle";

        public static const BASE_USER:String = "baseUser";

        public static const BASE_CLAN:String = "baseClan";

        public static const BATTLE_RESULTS_USER:String = "battleResultsUser";

        public static const PREBATTLE_USER:String = "prebattleUser";

        public static const UNIT_USER:String = "unitUser";

        public static const FORT_BUILDING:String = "fortBuilding";

        public static const TECHNICAL_MAINTENANCE:String = "technicalMaintenance";

        public static const CONTACTS_GROUP:String = "contactsGroup";

        public static const PLAYER_CONTACTS:String = "playerContacts";

        public static const STORE_VEHICLE:String = "storeVehicle";

        public static const PROFILE_VEHICLE:String = "profileVehicle";

        public static const VEH_COMPARE:String = "vehCompare";

        public static const CUSTOM_USER:String = "customUser";

        public static const CUSTOMIZATION_ITEM:String = "customizationItem";

        public static const STRONGHOLD_USER:String = "strongholdUser";

        public static const INGAME_SHOP:String = "ingameShop";

        public static const STORAGE_FOR_SELL_ITEM:String = "storageForSellItem";

        public static const STORAGE_MODULES_SHELLS_ITEM:String = "storageModulesShellsItem";

        public static const STORAGE_EQUIPMENT_ITEM:String = "storageEquipmentItem";

        public static const STORAGE_BONS_ITEM:String = "storageBonsItem";

        public static const STORAGE_VEHICLES_REGULAR_ITEM:String = "storageVehiclesRegularItem";

        public static const STORAGE_VEHICLES_RESTORE_ITEM:String = "storageVehiclesRestoreItem";

        public static const STORAGE_VEHICLES_RENTED_ITEM:String = "storageVehiclesRentedItem";

        public static const STORAGE_VEHICLES_MULTI_NATION_ITEM:String = "storageVehiclesMultiNationItem";

        public static const STORAGE_PERSONAL_RESERVE_ITEM:String = "storagePersonalReserveItem";

        public static const STORAGE_CUSTOMZIZATION_ITEM:String = "storageCustomizationItem";

        public static const STORAGE_BLUEPRINTS_ITEM:String = "storageBlueprintsItem";

        public static const STORAGE_CREW_BOOKS_ITEM:String = "storageCrewBooksItem";

        public static const STORAGE_CREW_BOOKS_NO_SALE_ITEM:String = "storageCrewBooksNoSaleItem";

        public function CONTEXT_MENU_HANDLER_TYPE()
        {
            super();
        }
    }
}
