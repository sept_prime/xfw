package net.wg.data.constants.generated
{
    public class CURRENCIES_CONSTANTS extends Object
    {

        public static const GOLD:String = "gold";

        public static const CREDITS:String = "credits";

        public static const CRYSTAL:String = "crystal";

        public static const XP_COST:String = "xp";

        public static const FREE_XP:String = "freeXP";

        public static const ERROR:String = "error";

        public static const GOLD_COLOR:int = 16761699;

        public static const CREDITS_COLOR:int = 13556185;

        public static const CRYSTAL_COLOR:int = 13224374;

        public static const CREDITS_INDEX:int = 0;

        public static const GOLD_INDEX:int = 1;

        public static const CRYSTAL_INDEX:int = 2;

        public function CURRENCIES_CONSTANTS()
        {
            super();
        }
    }
}
