package net.wg.data.constants.generated
{
    public class APP_CONTAINERS_NAMES extends Object
    {

        public static const VIEWS:String = "view";

        public static const TOP_SUB_VIEW:String = "topSubView";

        public static const SUBVIEW:String = "subView";

        public static const WINDOWS:String = "window";

        public static const SYSTEM_MESSAGES:String = "systemMessages";

        public static const BROWSER:String = "browser";

        public static const DIALOGS:String = "topWindow";

        public static const OVERLAY:String = "overlay";

        public static const IME:String = "ime";

        public static const SERVICE_LAYOUT:String = "serviceLayout";

        public static const TOOL_TIPS:String = "toolTips";

        public static const CURSOR:String = "cursor";

        public static const WAITING:String = "waiting";

        public static const MARKER:String = "marker";

        public static const CONTAINER_TYPES:Array = [VIEWS,TOP_SUB_VIEW,SUBVIEW,WINDOWS,SYSTEM_MESSAGES,BROWSER,DIALOGS,OVERLAY,IME,SERVICE_LAYOUT,TOOL_TIPS,CURSOR,WAITING,MARKER];

        public static const FOCUS_ORDER:Array = [WAITING,SERVICE_LAYOUT,OVERLAY,DIALOGS,BROWSER,WINDOWS,TOP_SUB_VIEW,SUBVIEW,VIEWS,MARKER];

        public function APP_CONTAINERS_NAMES()
        {
            super();
        }
    }
}
