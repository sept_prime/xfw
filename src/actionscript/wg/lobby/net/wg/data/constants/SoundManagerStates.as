package net.wg.data.constants
{
    public class SoundManagerStates extends Object
    {

        public static const SND_OVER:String = "over";

        public static const SND_OUT:String = "out";

        public static const SND_PRESS:String = "press";

        public function SoundManagerStates()
        {
            super();
        }
    }
}
