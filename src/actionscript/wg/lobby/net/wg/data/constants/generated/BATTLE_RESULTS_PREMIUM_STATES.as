package net.wg.data.constants.generated
{
    public class BATTLE_RESULTS_PREMIUM_STATES extends Object
    {

        public static const PREMIUM_INFO:int = 0;

        public static const PREMIUM_BONUS:int = 1;

        public static const PREMIUM_EARNINGS:int = 2;

        public static const PREMIUM_ADVERTISING:int = 3;

        public function BATTLE_RESULTS_PREMIUM_STATES()
        {
            super();
        }
    }
}
