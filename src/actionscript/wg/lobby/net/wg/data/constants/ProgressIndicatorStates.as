package net.wg.data.constants
{
    public class ProgressIndicatorStates extends Object
    {

        public static const CURRENT:String = "current";

        public static const STRATEGIC:String = "strategic";

        public static const COMMON:String = "common";

        public function ProgressIndicatorStates()
        {
            super();
        }
    }
}
