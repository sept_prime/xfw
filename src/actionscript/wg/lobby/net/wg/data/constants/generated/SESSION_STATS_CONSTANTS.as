package net.wg.data.constants.generated
{
    public class SESSION_STATS_CONSTANTS extends Object
    {

        public static const SESSION_STATS_POPOVER:String = "SessionStatsPopover";

        public static const SESSION_BATTLE_STATS_VIEW_PY_ALIAS:String = "SessionBattleStatsView";

        public static const SESSION_BATTLE_STATS_VIEW_LINKAGE:String = "SessionBattleStatsViewUI";

        public static const SESSION_VEHICLE_STATS_VIEW_PY_ALIAS:String = "SessionVehicleStatsView";

        public static const SESSION_VEHICLE_STATS_VIEW_LINKAGE:String = "SessionVehicleStatsViewUI";

        public static const SESSION_STATS_BUTTON_ALIAS:String = "SessionStatsButtonAlias";

        public static const SESSION_STATS_PROPS_WTR:String = "wtr";

        public static const SESSION_STATS_PROPS_RATIO_DAMAGE:String = "ratioDamage";

        public static const SESSION_STATS_PROPS_RATIO_KILL:String = "ratioKill";

        public static const SESSION_STATS_PROPS_RATIO_WIN:String = "winRatio";

        public static const SESSION_STATS_PROPS_AVERAGE_DAMAGE:String = "averageDamage";

        public static const SESSION_STATS_PROPS_HELP_DAMAGE:String = "helpDamage";

        public static const SESSION_STATS_PROPS_BLOCKED_DAMAGE:String = "blockedDamage";

        public static const SESSION_STATS_PROPS_AVERAGE_XP:String = "averageXp";

        public static const SESSION_STATS_PROPS_AVERAGE_ASSIST:String = "helpDamage";

        public static const SESSION_STATS_PROPS_INCOME_CREDITS:String = "incomeCredits";

        public static const SESSION_STATS_PROPS_XP:String = "xp";

        public static const SESSION_STATS_PROPS_INCOME_CRYSTAL:String = "incomeCrystal";

        public static const SESSION_STATS_PROPS_FREE_XP:String = "freeXp";

        public static const SESSION_STATS_PROPS_NET_CREDITS:String = "netCredits";

        public static const SESSION_STATS_PROPS_NET_CRYSTAL:String = "netCrystal";

        public function SESSION_STATS_CONSTANTS()
        {
            super();
        }
    }
}
