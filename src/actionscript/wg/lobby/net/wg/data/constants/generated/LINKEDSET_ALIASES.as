package net.wg.data.constants.generated
{
    public class LINKEDSET_ALIASES extends Object
    {

        public static const LINKED_SET_DETAILS_CONTAINER_VIEW:String = "LinkedSetDetailsContainerView";

        public static const LINKED_SET_DETAILS_VIEW:String = "LinkedSetDetailsViewUI";

        public static const LINKED_SET_DETAILS_VIEW_LINKAGE:String = "LinkedSetDetailsViewUI";

        public static const LINKED_SET_DETAILS_VIEW_ALIAS:String = "LinkedSetDetailsView";

        public static const LINKED_SET_VEHICLE_LIST_POPUP_PY:String = "LinkedSetVehicleListPopup";

        public static const LINKED_SET_VEHICLE_LIST_POPUP_UI:String = "vehicleListPopup.swf";

        public function LINKEDSET_ALIASES()
        {
            super();
        }
    }
}
