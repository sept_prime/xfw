package net.wg.data.constants.generated
{
    public class ACTION_PRICE_CONSTANTS extends Object
    {

        public static const STATE_ALIGN_MIDDLE:String = "alignMiddle";

        public static const STATE_ALIGN_MIDDLE_SMALL:String = "alignMiddleSmall";

        public static const STATE_ALIGN_TOP:String = "alignTop";

        public static const STATE_CAMOUFLAGE:String = "camouflage";

        public function ACTION_PRICE_CONSTANTS()
        {
            super();
        }
    }
}
