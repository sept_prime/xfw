package net.wg.data.constants.generated
{
    public class ROOT_SWF_CONSTANTS extends Object
    {

        public static const BATTLE_VEHICLE_MARKERS_REGISTER_CALLBACK:String = "registerMarkersManager";

        public static const BATTLE_CROSSHAIRS_REGISTER_CALLBACK:String = "registerCrosshairPanel";

        public static const GAME_LOADING_REGISTER_CALLBACK:String = "registerGameLoading";

        public static const BOOTCAMP_TRANISITION_CALLBACK:String = "registerBootcampTransition";

        public static const WAITING_TRANSITION_CALLBACK:String = "registerWaitingTransition";

        public function ROOT_SWF_CONSTANTS()
        {
            super();
        }
    }
}
