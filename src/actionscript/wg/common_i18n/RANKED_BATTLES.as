package
{
    public class RANKED_BATTLES extends Object
    {

        public static const TOOLTIP_CLOSEBUTTON:String = "#ranked_battles:tooltip/closeButton";

        public static const REWARDSVIEW_TABS_YEAR_SCOREPOINT_TOOLTIP:String = "#ranked_battles:rewardsView/tabs/year/scorePoint/tooltip";

        public static const RANKEDBATTLEPAGE_CLOSEBTN:String = "#ranked_battles:rankedBattlePage/closeBtn";

        public static const RANKEDBATTLE_TITLE:String = "#ranked_battles:rankedBattle/title";

        public static const RANKEDBATTLEMAINVIEW_SEASON:String = "#ranked_battles:rankedBattleMainView/season";

        public static const RANKEDBATTLEMAINVIEW_DATE_PERIOD:String = "#ranked_battles:rankedBattleMainView/date/period";

        public static const RANKEDBATTLEMAINVIEW_DATE_DAYS:String = "#ranked_battles:rankedBattleMainView/date/days";

        public static const RANKEDBATTLEMAINVIEW_DATE_HOURS:String = "#ranked_battles:rankedBattleMainView/date/hours";

        public static const RANKEDBATTLEMAINVIEW_DATE_MIN:String = "#ranked_battles:rankedBattleMainView/date/min";

        public static const RANKEDBATTLEMAINVIEW_DATE_LESSMIN:String = "#ranked_battles:rankedBattleMainView/date/lessMin";

        public static const RANKEDBATTLEMAINVIEW_SEASONCOMPLETE:String = "#ranked_battles:rankedBattleMainView/seasonComplete";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP:String = "#ranked_battles:rankedBattleMainView/seasonGap";

        public static const RANKEDBATTLEMAINVIEW_BONUSBATTLES:String = "#ranked_battles:rankedBattleMainView/bonusBattles";

        public static const RANKEDBATTLEMAINVIEW_INFOPAGE_HEADER:String = "#ranked_battles:rankedBattleMainView/infoPage/header";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_UNAVAILABLETITLE:String = "#ranked_battles:rankedBattleMainView/leaguesView/unavailableTitle";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_DESCR:String = "#ranked_battles:rankedBattleMainView/leaguesView/descr";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_UNAVAILABLEDESCR:String = "#ranked_battles:rankedBattleMainView/leaguesView/unavailableDescr";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_LEAGUE1:String = "#ranked_battles:rankedBattleMainView/leaguesView/league1";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_LEAGUE2:String = "#ranked_battles:rankedBattleMainView/leaguesView/league2";

        public static const RANKEDBATTLEMAINVIEW_LEAGUESVIEW_LEAGUE3:String = "#ranked_battles:rankedBattleMainView/leaguesView/league3";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_LEAGUE1:String = "#ranked_battles:rankedBattleMainView/seasonGap/league1";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_LEAGUE2:String = "#ranked_battles:rankedBattleMainView/seasonGap/league2";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_LEAGUE3:String = "#ranked_battles:rankedBattleMainView/seasonGap/league3";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_SPRINTER:String = "#ranked_battles:rankedBattleMainView/seasonGap/sprinter";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_DIVISION_TITLE:String = "#ranked_battles:rankedBattleMainView/seasonGap/division/title";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_WAITINGLEAGUE_TITLE:String = "#ranked_battles:rankedBattleMainView/seasonGap/waitingLeague/title";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_NOTINSEASON_TITLE:String = "#ranked_battles:rankedBattleMainView/seasonGap/notInSeason/title";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_NOTINDIVISIONS_TITLE:String = "#ranked_battles:rankedBattleMainView/seasonGap/notInDivisions/title";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_LEAGUE_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/league/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_DIVISION_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/division/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_WAITINGLEAGUE_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/waitingLeague/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_NOTINSEASON_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/notInSeason/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_NOTINDIVISIONS_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/notInDivisions/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_BANNEDLEAGUE_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/bannedLeague/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_BANNEDDIVISION_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/bannedDivision/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_BANNEDNOTINSEASON_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/bannedNotInSeason/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_BANNEDNOTINDIVISIONS_DESCR:String = "#ranked_battles:rankedBattleMainView/seasonGap/bannedNotInDivisions/descr";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_LEAGUE_RATINGBTN:String = "#ranked_battles:rankedBattleMainView/seasonGap/league/ratingBtn";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_DIVISION_RATINGBTN:String = "#ranked_battles:rankedBattleMainView/seasonGap/division/ratingBtn";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_STATS_EFFICIENCY:String = "#ranked_battles:rankedBattleMainView/seasonGap/stats/efficiency";

        public static const RANKEDBATTLEMAINVIEW_SEASONGAP_STATS_OUTOFRATING:String = "#ranked_battles:rankedBattleMainView/seasonGap/stats/outOfRating";

        public static const RANKEDBATTLEMAINVIEW_STATS_STRIPESINLEAGUE:String = "#ranked_battles:rankedBattleMainView/stats/stripesInLeague";

        public static const RANKEDBATTLEMAINVIEW_STATS_BATTLESINLEAGUE:String = "#ranked_battles:rankedBattleMainView/stats/battlesInLeague";

        public static const RANKEDBATTLEMAINVIEW_STATS_STRIPESTOTAL:String = "#ranked_battles:rankedBattleMainView/stats/stripesTotal";

        public static const RANKEDBATTLEMAINVIEW_STATS_BATTLESTOTAL:String = "#ranked_battles:rankedBattleMainView/stats/battlesTotal";

        public static const RANKEDBATTLEMAINVIEW_STATS_SEASONEFFICIENCY:String = "#ranked_battles:rankedBattleMainView/stats/seasonEfficiency";

        public static const RANKEDBATTLEMAINVIEW_STATS_RATING_TITLE:String = "#ranked_battles:rankedBattleMainView/stats/rating/title";

        public static const RANKEDBATTLEMAINVIEW_STATS_RATING_TOOLTIP:String = "#ranked_battles:rankedBattleMainView/stats/rating/tooltip";

        public static const RANKEDBATTLEMAINVIEW_STATS_RATING_UNAVAILABLETOOLTIP:String = "#ranked_battles:rankedBattleMainView/stats/rating/unavailableTooltip";

        public static const RANKEDBATTLEMAINVIEW_STATS_DIVISIONEFFICIENCY:String = "#ranked_battles:rankedBattleMainView/stats/divisionEfficiency";

        public static const RANKEDBATTLEMAINVIEW_STATS_QUALIFICATIONEFFICIENCY:String = "#ranked_battles:rankedBattleMainView/stats/qualificationEfficiency";

        public static const RANKEDBATTLEMAINVIEW_STATS_QUALIFICATIONSTEPS:String = "#ranked_battles:rankedBattleMainView/stats/qualificationSteps";

        public static const RANKEDBATTLEMAINVIEW_DIVISIONS_RANK:String = "#ranked_battles:rankedBattleMainView/divisions/rank";

        public static const RANKEDBATTLEMAINVIEW_DIVISIONS_CURRENTRANK:String = "#ranked_battles:rankedBattleMainView/divisions/currentRank";

        public static const RANKEDBATTLEVIEW_TITLE:String = "#ranked_battles:rankedBattleView/title";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTS_HEADER:String = "#ranked_battles:rankedBattleView/awardBlock/points/header";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTS_CURRLEAGUEHEADER:String = "#ranked_battles:rankedBattleView/awardBlock/points/currLeagueHeader";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTS_NOLEAGUEHEADER:String = "#ranked_battles:rankedBattleView/awardBlock/points/noLeagueHeader";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTS_NOPRISEHEADER:String = "#ranked_battles:rankedBattleView/awardBlock/points/noPriseHeader";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTSALT_DESCRIPTION:String = "#ranked_battles:rankedBattleView/awardBlock/pointsAlt/description";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_POINTS_TOTALLABEL:String = "#ranked_battles:rankedBattleView/awardBlock/points/totalLabel";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_EMPTYLEAGUE:String = "#ranked_battles:rankedBattleView/awardBlock/emptyLeague";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_QUEST_HEADER:String = "#ranked_battles:rankedBattleView/awardBlock/quest/header";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_QUEST_DESCRIPTION_BATTLESLEFT:String = "#ranked_battles:rankedBattleView/awardBlock/quest/description/battlesLeft";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_QUEST_DESCRIPTION_NOQUEST:String = "#ranked_battles:rankedBattleView/awardBlock/quest/description/noQuest";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_SEASONAWARD_HEADER:String = "#ranked_battles:rankedBattleView/awardBlock/seasonAward/header";

        public static const RANKEDBATTLEVIEW_AWARDBLOCK_SEASONAWARD_DESCRIPTION:String = "#ranked_battles:rankedBattleView/awardBlock/seasonAward/description";

        public static const RANKEDBATTLEVIEW_NOTRANKEDBLOCK_OKTOOLTIP_HEADER:String = "#ranked_battles:rankedBattleView/notRankedBlock/okTooltip/header";

        public static const RANKEDBATTLEVIEW_NOTRANKEDBLOCK_OKTOOLTIP_BODY:String = "#ranked_battles:rankedBattleView/notRankedBlock/okTooltip/body";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_CURRENTRANK:String = "#ranked_battles:rankedBattleView/progressBlock/currentRank";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_VEHICLERANK:String = "#ranked_battles:rankedBattleView/progressBlock/vehicleRank";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_STEPS:String = "#ranked_battles:rankedBattleView/progressBlock/steps";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_BESTRANK:String = "#ranked_battles:rankedBattleView/progressBlock/bestRank";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_FINALRANK:String = "#ranked_battles:rankedBattleView/progressBlock/finalRank";

        public static const RANKEDBATTLEVIEW_PROGRESSBLOCK_CONTINUE:String = "#ranked_battles:rankedBattleView/progressBlock/continue";

        public static const RANKEDBATTLEVIEW_STATUSBLOCK_STATUSTEXT:String = "#ranked_battles:rankedBattleView/statusBlock/statusText";

        public static const RANKEDBATTLEVIEW_STATUSBLOCK_CALENDARBTNTOOLTIP_HEADER:String = "#ranked_battles:rankedBattleView/statusBlock/calendarBtnTooltip/header";

        public static const RANKEDBATTLEVIEW_STATUSBLOCK_CALENDARBTNTOOLTIP_BODY:String = "#ranked_battles:rankedBattleView/statusBlock/calendarBtnTooltip/body";

        public static const RANKEDBATTLEVIEW_STATUSBLOCK_CALENDARPOPOVER_CYCLEITEM:String = "#ranked_battles:rankedBattleView/statusBlock/calendarPopover/cycleItem";

        public static const RANKEDBATTLEVIEW_STATUSBLOCK_CALENDARPOPOVER_ATTENTIONTEXT:String = "#ranked_battles:rankedBattleView/statusBlock/calendarPopover/attentionText";

        public static const RANKEDBATTLEVIEW_CLOSEBTN:String = "#ranked_battles:rankedBattleView/closeBtn";

        public static const RANKEDBATTLEVIEW_CLOSEBTNDESCR:String = "#ranked_battles:rankedBattleView/closeBtnDescr";

        public static const RANKEDBATTLEVIEW_PLAYVIDEOBTN:String = "#ranked_battles:rankedBattleView/playVideoBtn";

        public static const RANKEDBATTLEMAINVIEW_EMPTYBUBBLE:String = "#ranked_battles:rankedBattleMainView/emptyBubble";

        public static const RANKEDAWARDS_AWARDSBLOCK_TITLE:String = "#ranked_battles:rankedAwards/awardsBlock/title";

        public static const RANKEDAWARDS_AWARDSBLOCK_DESCRIPTION:String = "#ranked_battles:rankedAwards/awardsBlock/description";

        public static const RANKEDAWARDS_AWARDSBLOCK_POINTS:String = "#ranked_battles:rankedAwards/awardsBlock/points";

        public static const RANKEDAWARDS_AWARDSBLOCK_CRYSTAL:String = "#ranked_battles:rankedAwards/awardsBlock/crystal";

        public static const RANKEDAWARDS_AWARDSBLOCK_AWARDSHEADERTXT:String = "#ranked_battles:rankedAwards/awardsBlock/awardsHeaderTxt";

        public static const RANKEDBATTLEVIEW_NEXTSTAGEBLOCK_STAGE:String = "#ranked_battles:rankedBattleView/nextStageBlock/stage";

        public static const RANKEDBATTLESCYCLESVIEW_NOACHIEVEMENTS_PAST_FOOTER:String = "#ranked_battles:rankedBattlesCyclesView/noAchievements/past/footer";

        public static const RANKEDBATTLESCYCLESVIEW_NOACHIEVEMENTS_FUTURE_FOOTER:String = "#ranked_battles:rankedBattlesCyclesView/noAchievements/future/footer";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_TABLEHEADER_RANK:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/tableHeader/rank";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_TABLEHEADER_RANKAWARD:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/tableHeader/rankAward";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_VEHICLERANKCOMMENT:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/vehicleRankComment";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_TOOLTIP_CYCLEAWARD_HEADER:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/tooltip/cycleAward/header";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_TOOLTIP_CYCLEAWARD_BODY:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/tooltip/cycleAward/body";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_ITEM_AWARDRECIEVED:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/item/awardRecieved";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_ITEM_BOXTOOLTIP_HEADER:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/item/boxTooltip/header";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_ITEM_BOXTOOLTIP_BODY:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/item/boxTooltip/body";

        public static const RANKEDBATTLESCYCLESVIEW_AWARDBLOCK_BOXTOOLTIP_HEADER:String = "#ranked_battles:rankedBattlesCyclesView/awardBlock/boxTooltip/header";

        public static const RANKEDBATTLESCYCLESVIEW_AWARDBLOCK_BOXTOOLTIP_BODY:String = "#ranked_battles:rankedBattlesCyclesView/awardBlock/boxTooltip/body";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_ITEM_CRYSTALTOOLTIP_HEADER:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/item/crystalTooltip/header";

        public static const RANKEDBATTLESCYCLESVIEW_CURRENTCYCLE_ITEM_CRYSTALTOOLTIP_BODY:String = "#ranked_battles:rankedBattlesCyclesView/currentCycle/item/crystalTooltip/body";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_TABLEHEADER_POSITION:String = "#ranked_battles:rankedBattlesCyclesView/final/tableHeader/position";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_TABLEHEADER_SEASONAWARD:String = "#ranked_battles:rankedBattlesCyclesView/final/tableHeader/seasonAward";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_TABLEHEADER_NOLEAGUEDATATOOLTIP:String = "#ranked_battles:rankedBattlesCyclesView/final/tableHeader/noLeagueDataTooltip";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_BTNLADDER:String = "#ranked_battles:rankedBattlesCyclesView/final/btnLadder";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_BTNLADDERTOOLTIP_HEADER:String = "#ranked_battles:rankedBattlesCyclesView/final/btnLadderTooltip/header";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_BTNLADDERTOOLTIP_BODY:String = "#ranked_battles:rankedBattlesCyclesView/final/btnLadderTooltip/body";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_FIRST:String = "#ranked_battles:rankedBattlesCyclesView/final/first";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_INTERMEDIATE:String = "#ranked_battles:rankedBattlesCyclesView/final/intermediate";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_LAST:String = "#ranked_battles:rankedBattlesCyclesView/final/last";

        public static const RANKEDBATTLESCYCLESVIEW_FINAL_LASTCOMMENT:String = "#ranked_battles:rankedBattlesCyclesView/final/lastComment";

        public static const RANKEDBATTLESCYCLESVIEW_BTNCLOSE:String = "#ranked_battles:rankedBattlesCyclesView/btnClose";

        public static const RANKEDBATTLESWIDGET_QUALIFICATIONIDLETEXT:String = "#ranked_battles:rankedBattlesWidget/qualificationIdleText";

        public static const RANKEDBATTLESWIDGET_NEWRANKCONGRAT:String = "#ranked_battles:rankedBattlesWidget/newRankCongrat";

        public static const RANKEDBATTLESWIDGET_NEWDIVISIONCONGRAT_TITLE:String = "#ranked_battles:rankedBattlesWidget/newDivisionCongrat/title";

        public static const RANKEDBATTLESWIDGET_NEWDIVISIONCONGRAT_INFO:String = "#ranked_battles:rankedBattlesWidget/newDivisionCongrat/info";

        public static const RANKEDBATTLESWIDGET_LEAGUESACHIEVMENT_TITLE:String = "#ranked_battles:rankedBattlesWidget/leaguesAchievment/title";

        public static const RANKEDBATTLESWIDGET_LEAGUESACHIEVMENT_INFO:String = "#ranked_battles:rankedBattlesWidget/leaguesAchievment/info";

        public static const RANKEDBATTLESWIDGET_LEAGUEINCREASE_TITLE:String = "#ranked_battles:rankedBattlesWidget/leagueIncrease/title";

        public static const RANKEDBATTLESWIDGET_LEAGUEINCREASE_INFO:String = "#ranked_battles:rankedBattlesWidget/leagueIncrease/info";

        public static const RANKEDBATTLESWIDGET_LEAGUEDECREASE_TITLE:String = "#ranked_battles:rankedBattlesWidget/leagueDecrease/title";

        public static const RANKEDBATTLESWIDGET_LEAGUEDECREASE_INFO:String = "#ranked_battles:rankedBattlesWidget/leagueDecrease/info";

        public static const RANKEDBATTLESWIDGET_BONUSMULTIPLIER:String = "#ranked_battles:rankedBattlesWidget/bonusMultiplier";

        public static const RANKEDBATTLESWIDGET_BONUSSTEPS:String = "#ranked_battles:rankedBattlesWidget/bonusSteps";

        public static const RANKEDBATTLESWIDGET_LEAGUE1:String = "#ranked_battles:rankedBattlesWidget/league1";

        public static const RANKEDBATTLESWIDGET_LEAGUE2:String = "#ranked_battles:rankedBattlesWidget/league2";

        public static const RANKEDBATTLESWIDGET_LEAGUE3:String = "#ranked_battles:rankedBattlesWidget/league3";

        public static const RANKEDBATTLESWIDGET_LEAGUE0:String = "#ranked_battles:rankedBattlesWidget/league0";

        public static const RANKEDBATTLESWIDGET_NOCHANGES:String = "#ranked_battles:rankedBattlesWidget/noChanges";

        public static const WELCOMESCREEN_HEADER:String = "#ranked_battles:welcomeScreen/header";

        public static const WELCOMESCREEN_LEFTRULE:String = "#ranked_battles:welcomeScreen/leftRule";

        public static const WELCOMESCREEN_LEFTRULETOOLTIP_HEADER:String = "#ranked_battles:welcomeScreen/leftRuleTooltip/header";

        public static const WELCOMESCREEN_LEFTRULETOOLTIP_BODY:String = "#ranked_battles:welcomeScreen/leftRuleTooltip/body";

        public static const WELCOMESCREEN_CENTERRULE:String = "#ranked_battles:welcomeScreen/centerRule";

        public static const WELCOMESCREEN_CENTERRULETOOLTIP_HEADER:String = "#ranked_battles:welcomeScreen/centerRuleTooltip/header";

        public static const WELCOMESCREEN_CENTERRULETOOLTIP_BODY:String = "#ranked_battles:welcomeScreen/centerRuleTooltip/body";

        public static const WELCOMESCREEN_RIGHTRULE:String = "#ranked_battles:welcomeScreen/rightRule";

        public static const WELCOMESCREEN_RIGHTRULETOOLTIP_HEADER:String = "#ranked_battles:welcomeScreen/rightRuleTooltip/header";

        public static const WELCOMESCREEN_RIGHTRULETOOLTIP_BODY:String = "#ranked_battles:welcomeScreen/rightRuleTooltip/body";

        public static const WELCOMESCREEN_RANKDESCR:String = "#ranked_battles:welcomeScreen/rankDescr";

        public static const WELCOMESCREEN_RANKSDESCR:String = "#ranked_battles:welcomeScreen/ranksDescr";

        public static const WELCOMESCREEN_RANKS_HEADER:String = "#ranked_battles:welcomeScreen/ranks/header";

        public static const WELCOMESCREEN_POSITIVE_BODY:String = "#ranked_battles:welcomeScreen/positive/body";

        public static const WELCOMESCREEN_POSITIVE_TEAM:String = "#ranked_battles:welcomeScreen/positive/team";

        public static const WELCOMESCREEN_NEGATIVE_BODY:String = "#ranked_battles:welcomeScreen/negative/body";

        public static const WELCOMESCREEN_NEGATIVE_TEAM:String = "#ranked_battles:welcomeScreen/negative/team";

        public static const WELCOMESCREEN_BTN:String = "#ranked_battles:welcomeScreen/btn";

        public static const WELCOMESCREEN_RULESDELIMETER:String = "#ranked_battles:welcomeScreen/rulesDelimeter";

        public static const WELCOMESCREEN_EQUALITYTEXT:String = "#ranked_battles:welcomeScreen/equalityText";

        public static const WELCOMESCREEN_CLOSEBTN:String = "#ranked_battles:welcomeScreen/closeBtn";

        public static const WELCOMESCREEN_CLOSEBTNTOOLTIP_HEADER:String = "#ranked_battles:welcomeScreen/closeBtnTooltip/header";

        public static const WELCOMESCREEN_CLOSEBTNTOOLTIP_BODY:String = "#ranked_battles:welcomeScreen/closeBtnTooltip/body";

        public static const RANKEDBATTLESUNREACHABLEVIEW_SUBTITLETEXT:String = "#ranked_battles:rankedBattlesUnreachableView/subtitleText";

        public static const RANKEDBATTLESUNREACHABLEVIEW_UNREACHABLETEXT:String = "#ranked_battles:rankedBattlesUnreachableView/unreachableText";

        public static const RANKEDBATTLESUNREACHABLEVIEW_CLOSEBTNLABEL:String = "#ranked_battles:rankedBattlesUnreachableView/closeBtnLabel";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOMTEXT:String = "#ranked_battles:rankedBattlesUnreachableView/bottomText";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOM_PREMIUM:String = "#ranked_battles:rankedBattlesUnreachableView/bottom/premium";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOM_PREMIUM_BASIC:String = "#ranked_battles:rankedBattlesUnreachableView/bottom/premium/basic";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOM_PREMIUM_PLUS:String = "#ranked_battles:rankedBattlesUnreachableView/bottom/premium/plus";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOM_MISSIONS:String = "#ranked_battles:rankedBattlesUnreachableView/bottom/missions";

        public static const RANKEDBATTLESUNREACHABLEVIEW_BOTTOM_RESERVES:String = "#ranked_battles:rankedBattlesUnreachableView/bottom/reserves";

        public static const RANKEDBATTLECYCLESVIEW_TITLE:String = "#ranked_battles:rankedBattleCyclesView/title";

        public static const RANKEDBATTLEHEADER_POINTS_PAST:String = "#ranked_battles:rankedBattleHeader/points/past";

        public static const RANKEDBATTLEHEADER_POINTS_CURRENT:String = "#ranked_battles:rankedBattleHeader/points/current";

        public static const RANKEDBATTLEHEADER_POINTS_FINAL:String = "#ranked_battles:rankedBattleHeader/points/final";

        public static const RANKEDBATTLECYCLESVIEW_TABS_RANKS:String = "#ranked_battles:rankedBattleCyclesView/tabs/ranks";

        public static const RANKEDBATTLECYCLESVIEW_TABS_FINAL:String = "#ranked_battles:rankedBattleCyclesView/tabs/final";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_PAST_HEADER:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/past/header";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_PAST_BODY:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/past/body";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_CURRENT_HEADER:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/current/header";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_CURRENT_BODY:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/current/body";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_FINAL_HEADER:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/final/header";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_FINAL_BODY:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/final/body";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_FUTURE_HEADER:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/future/header";

        public static const RANKEDBATTLEHEADER_TOOLTIP_CYCLE_FUTURE_BODY:String = "#ranked_battles:rankedBattleHeader/tooltip/cycle/future/body";

        public static const RANKEDBATTLEHEADER_TOOLTIP_DATE:String = "#ranked_battles:rankedBattleHeader/tooltip/date";

        public static const BATTLERESULT_STAGESEARNED:String = "#ranked_battles:battleresult/stagesEarned";

        public static const BATTLERESULT_STAGEEARNED:String = "#ranked_battles:battleresult/stageEarned";

        public static const BATTLERESULT_STAGENOTEARNED:String = "#ranked_battles:battleresult/stageNotEarned";

        public static const BATTLERESULT_STAGESAVED:String = "#ranked_battles:battleresult/stageSaved";

        public static const BATTLERESULT_RANKUNBURNABLE:String = "#ranked_battles:battleresult/rankUnburnable";

        public static const BATTLERESULT_STAGELOST:String = "#ranked_battles:battleresult/stageLost";

        public static const BATTLERESULT_RANKEARNED:String = "#ranked_battles:battleresult/rankEarned";

        public static const BATTLERESULT_DIVISIONEARNED:String = "#ranked_battles:battleresult/divisionEarned";

        public static const BATTLERESULT_LEAGUEEARNED:String = "#ranked_battles:battleresult/leagueEarned";

        public static const BATTLERESULT_RANKLOST:String = "#ranked_battles:battleresult/rankLost";

        public static const BATTLERESULT_SHIELDLOSE:String = "#ranked_battles:battleresult/shieldLose";

        public static const BATTLERESULT_SHIELDLOSESTEP:String = "#ranked_battles:battleresult/shieldLoseStep";

        public static const BATTLERESULT_FIRST_INTOP_LOSE:String = "#ranked_battles:battleresult/first/inTop/lose";

        public static const BATTLERESULT_FIRST_INTOP_WIN:String = "#ranked_battles:battleresult/first/inTop/win";

        public static const BATTLERESULT_FIRST_NOTINTOP_LOSE:String = "#ranked_battles:battleresult/first/notInTop/lose";

        public static const BATTLERESULT_FIRST_NOTINTOP_WIN:String = "#ranked_battles:battleresult/first/notInTop/win";

        public static const BATTLERESULT_INTOP_LOSE:String = "#ranked_battles:battleresult/inTop/lose";

        public static const BATTLERESULT_INTOP_WIN:String = "#ranked_battles:battleresult/inTop/win";

        public static const BATTLERESULT_NOTINTOP_LOSE:String = "#ranked_battles:battleresult/notInTop/lose";

        public static const BATTLERESULT_NOTINTOP_WIN:String = "#ranked_battles:battleresult/notInTop/win";

        public static const BATTLERESULT_BONUSBATTLESUSED:String = "#ranked_battles:battleresult/bonusBattlesUsed";

        public static const BATTLERESULT_NOTINTOP_STAGESAVED:String = "#ranked_battles:battleresult/notInTop/stageSaved";

        public static const BATTLERESULT_NOTINTOP_MINXP:String = "#ranked_battles:battleresult/notInTop/minXP";

        public static const BATTLERESULT_BONUSBATTLESEARNED:String = "#ranked_battles:battleresult/bonusBattlesEarned";

        public static const BATTLERESULT_BONUSBATTLESEARNED_TEXT:String = "#ranked_battles:battleresult/bonusBattlesEarned/text";

        public static const BATTLERESULT_LEAGUEUNAVAILABLE:String = "#ranked_battles:battleresult/leagueUnavailable";

        public static const BATTLERESULT_HEADERTEXT:String = "#ranked_battles:battleresult/headerText";

        public static const BATTLERESULT_STATUS_STAGESEARNED:String = "#ranked_battles:battleresult/status/stagesEarned";

        public static const BATTLERESULT_STATUS_STAGEEARNED:String = "#ranked_battles:battleresult/status/stageEarned";

        public static const BATTLERESULT_STATUS_RANKUNBURNABLE:String = "#ranked_battles:battleresult/status/rankUnburnable";

        public static const BATTLERESULT_STATUS_RANKEARNED:String = "#ranked_battles:battleresult/status/rankEarned";

        public static const BATTLERESULT_STATUS_STAGELOST:String = "#ranked_battles:battleresult/status/stageLost";

        public static const BATTLERESULT_STATUS_RANKLOST:String = "#ranked_battles:battleresult/status/rankLost";

        public static const BATTLERESULT_STATUS_SHIELDLOSESTEP:String = "#ranked_battles:battleresult/status/shieldLoseStep";

        public static const BATTLERESULT_STATUS_SHIELDLOSE:String = "#ranked_battles:battleresult/status/shieldLose";

        public static const BATTLERESULT_STATUS_SHIELDCOUNT:String = "#ranked_battles:battleresult/status/shieldCount";

        public static const BATTLERESULT_STATUS_SHIELDWARNING:String = "#ranked_battles:battleresult/status/shieldWarning";

        public static const BATTLERESULT_STATUS_SHIELDRENEW:String = "#ranked_battles:battleresult/status/shieldRenew";

        public static const BATTLERESULT_STATUS_STAGENOTEARNED:String = "#ranked_battles:battleresult/status/stageNotEarned";

        public static const BATTLERESULT_STATUS_STAGESAVED:String = "#ranked_battles:battleresult/status/stageSaved";

        public static const TOOLTIP_STEP_HEADER:String = "#ranked_battles:tooltip/step/header";

        public static const TOOLTIP_STEP_DESCRIPTION:String = "#ranked_battles:tooltip/step/description";

        public static const TOOLTIP_STEP_CONDITIONS_HEADER:String = "#ranked_battles:tooltip/step/conditions/header";

        public static const TOOLTIP_STEP_NOTCHARGED_HEADER:String = "#ranked_battles:tooltip/step/notCharged/header";

        public static const TOOLTIP_STEP_NOTCHARGED_DESCRIPTION:String = "#ranked_battles:tooltip/step/notCharged/description";

        public static const TOOLTIP_STEP_NOTCHARGEDLOSE_DESCRIPTION:String = "#ranked_battles:tooltip/step/notChargedLose/description";

        public static const TOOLTIP_STEP_DECOMMISSION_HEADER:String = "#ranked_battles:tooltip/step/decommission/header";

        public static const TOOLTIP_STEP_DECOMMISSION_DESCRIPTION:String = "#ranked_battles:tooltip/step/decommission/description";

        public static const TOOLTIP_STEP_DECOMMISSIONWIN_DESCRIPTION:String = "#ranked_battles:tooltip/step/decommissionWin/description";

        public static const TOOLTIP_STEP_FROMTILL:String = "#ranked_battles:tooltip/step/fromTill";

        public static const TOOLTIP_STEP_OR:String = "#ranked_battles:tooltip/step/or";

        public static const TOOLTIP_STEP_CONDITIONS:String = "#ranked_battles:tooltip/step/conditions";

        public static const TOOLTIP_STEP_WINNERS:String = "#ranked_battles:tooltip/step/winners";

        public static const TOOLTIP_STEP_LOSERS:String = "#ranked_battles:tooltip/step/losers";

        public static const TOOLTIP_CLOSEBUTTON_HEADER:String = "#ranked_battles:tooltip/closeButton/header";

        public static const TOOLTIP_CLOSEBUTTON_BODY:String = "#ranked_battles:tooltip/closeButton/body";

        public static const STATUS_TIMELEFT_DAYS:String = "#ranked_battles:status/timeLeft/days";

        public static const STATUS_TIMELEFT_HOURS:String = "#ranked_battles:status/timeLeft/hours";

        public static const STATUS_TIMELEFT_MIN:String = "#ranked_battles:status/timeLeft/min";

        public static const STATUS_TIMELEFT_LESSMIN:String = "#ranked_battles:status/timeLeft/lessMin";

        public static const BATTLERESULT_RANK_PROGRESS_UP:String = "#ranked_battles:battleResult/rank/progress/up";

        public static const BATTLERESULT_RANK_UP:String = "#ranked_battles:battleResult/rank/up";

        public static const BATTLERESULT_RANK_UNCHANGED:String = "#ranked_battles:battleResult/rank/unchanged";

        public static const BATTLERESULT_RANK_PROGRESS_LOSE:String = "#ranked_battles:battleResult/rank/progress/lose";

        public static const BATTLERESULT_RANK_LOSE:String = "#ranked_battles:battleResult/rank/lose";

        public static const BATTLERESULT_WINNERS:String = "#ranked_battles:battleResult/winners";

        public static const BATTLERESULT_LOSERS:String = "#ranked_battles:battleResult/losers";

        public static const BATTLERESULT_YES:String = "#ranked_battles:battleResult/yes";

        public static const AWARDS_YES:String = "#ranked_battles:awards/yes";

        public static const AWARDS_CONGRATULATION:String = "#ranked_battles:awards/congratulation";

        public static const AWARDS_GOTRANK:String = "#ranked_battles:awards/gotRank";

        public static const AWARDS_GOTQUALIFICATION:String = "#ranked_battles:awards/gotQualification";

        public static const AWARDS_GOTDIVISION:String = "#ranked_battles:awards/gotDivision";

        public static const AWARDS_GOTLEAGUE:String = "#ranked_battles:awards/gotLeague";

        public static const AWARDS_BONUSBATTLES_LABEL:String = "#ranked_battles:awards/bonusBattles/label";

        public static const AWARDS_BONUSBATTLES_HEADER:String = "#ranked_battles:awards/bonusBattles/header";

        public static const AWARDS_BONUSBATTLES_BODY:String = "#ranked_battles:awards/bonusBattles/body";

        public static const SEASONCOMPLETE_SMALLTITLE:String = "#ranked_battles:seasonComplete/smallTitle";

        public static const SEASONCOMPLETE_BIGTITLE:String = "#ranked_battles:seasonComplete/bigTitle";

        public static const SEASONCOMPLETE_EFFECTLABEL:String = "#ranked_battles:seasonComplete/effectLabel";

        public static const SEASONCOMPLETE_PLACEINRATING:String = "#ranked_battles:seasonComplete/placeInRating";

        public static const SEASONCOMPLETE_OUTOFRATING:String = "#ranked_battles:seasonComplete/outOfRating";

        public static const SEASONCOMPLETE_BESTRANK:String = "#ranked_battles:seasonComplete/bestRank";

        public static const SEASONCOMPLETE_LEADERSBUTTON:String = "#ranked_battles:seasonComplete/leadersButton";

        public static const SEASONCOMPLETE_SPRINTERTOP:String = "#ranked_battles:seasonComplete/sprinterTop";

        public static const SEASONCOMPLETE_SPRINTERIMPROVED:String = "#ranked_battles:seasonComplete/sprinterImproved";

        public static const SEASONCOMPLETE_DIVISION_TOOLTIP_HEADER:String = "#ranked_battles:seasonComplete/division/tooltip/header";

        public static const SEASONCOMPLETE_TOOLTIP_BODY:String = "#ranked_battles:seasonComplete/tooltip/body";

        public static const STATISTIC_PLAYERSRAITING:String = "#ranked_battles:statistic/playersRaiting";

        public static const STATISTIC_VEHICLERANK:String = "#ranked_battles:statistic/vehicleRank";

        public static const PRIMETIME_TITLE:String = "#ranked_battles:primeTime/title";

        public static const PRIMETIME_TITLEWELCOME:String = "#ranked_battles:primeTime/titleWelcome";

        public static const PRIMETIME_APPLYBTN:String = "#ranked_battles:primeTime/applyBtn";

        public static const PRIMETIME_CONTINUEBTN:String = "#ranked_battles:primeTime/continueBtn";

        public static const PRIMETIME_SERVERTOOLTIP:String = "#ranked_battles:primeTime/serverTooltip";

        public static const PRIMETIME_STATUS_SEASONDISABLED:String = "#ranked_battles:primeTime/status/seasonDisabled";

        public static const PRIMETIME_STATUS_DISABLEFIRST:String = "#ranked_battles:primeTime/status/disableFirst";

        public static const PRIMETIME_STATUS_DISABLE:String = "#ranked_battles:primeTime/status/disable";

        public static const PRIMETIME_STATUS_ALLSERVERSDISABLED:String = "#ranked_battles:primeTime/status/allServersDisabled";

        public static const PRIMETIME_STATUS_UNTILL:String = "#ranked_battles:primeTime/status/untill";

        public static const PRIMETIME_STATUS_PRIMEISAVAILABLE:String = "#ranked_battles:primeTime/status/primeIsAvailable";

        public static const PRIMETIME_STATUS_PRIMEWILLBEAVAILABLE:String = "#ranked_battles:primeTime/status/primeWillBeAvailable";

        public static const PRIMETIMEALERTMESSAGEBLOCK_MESSAGE:String = "#ranked_battles:primeTimeAlertMessageBlock/message";

        public static const PRIMETIME_TOOLTIP_SERVER_ONSERVER:String = "#ranked_battles:primeTime/tooltip/server/onServer";

        public static const PRIMETIME_TOOLTIP_SERVER_UNAVAILABLE_INTIME:String = "#ranked_battles:primeTime/tooltip/server/unavailable/inTime";

        public static const PRIMETIME_TOOLTIP_SERVER_AVAILABLE_UNTILL:String = "#ranked_battles:primeTime/tooltip/server/available/untill";

        public static const CALENDARDAY_TITLE:String = "#ranked_battles:calendarDay/title";

        public static const CALENDARDAY_SERVERNAME:String = "#ranked_battles:calendarDay/serverName";

        public static const CALENDARDAY_TIME:String = "#ranked_battles:calendarDay/time";

        public static const CALENDARSTEPSTOOLTIP_TITLE:String = "#ranked_battles:calendarStepsTooltip/title";

        public static const SELECTORTOOLTIP_TITLE:String = "#ranked_battles:selectorTooltip/title";

        public static const SELECTORTOOLTIP_DESC:String = "#ranked_battles:selectorTooltip/desc";

        public static const SELECTORTOOLTIP_TIMETABLE_TITLE:String = "#ranked_battles:selectorTooltip/timeTable/title";

        public static const SELECTORTOOLTIP_TIMETABLE_TODAY:String = "#ranked_battles:selectorTooltip/timeTable/today";

        public static const SELECTORTOOLTIP_TIMETABLE_TOMORROW:String = "#ranked_battles:selectorTooltip/timeTable/tomorrow";

        public static const SELECTORTOOLTIP_TIMETABLE_EMPTY:String = "#ranked_battles:selectorTooltip/timeTable/empty";

        public static const SELECTORTOOLTIP_TILLEND:String = "#ranked_battles:selectorTooltip/tillEnd";

        public static const NOTIFICATION_STAGEEARNED:String = "#ranked_battles:notification/stageEarned";

        public static const NOTIFICATION_STAGENOTEARNED:String = "#ranked_battles:notification/stageNotEarned";

        public static const NOTIFICATION_STAGELOST:String = "#ranked_battles:notification/stageLost";

        public static const NOTIFICATION_RANKEARNED:String = "#ranked_battles:notification/rankEarned";

        public static const NOTIFICATION_RANKLOST:String = "#ranked_battles:notification/rankLost";

        public static const NOTIFICATION_VEHICLERANKEARNED:String = "#ranked_battles:notification/vehicleRankEarned";

        public static const NOTIFICATION_VEHICLERANKLOST:String = "#ranked_battles:notification/vehicleRankLost";

        public static const NOTIFICATION_RANKEDMESSAGE:String = "#ranked_battles:notification/rankedMessage";

        public static const RANKEDBATTLESCAROUSEL_LOCKEDTOOLTIP_HEADER:String = "#ranked_battles:rankedBattlesCarousel/lockedTooltip/header";

        public static const RANKEDBATTLESCAROUSEL_LOCKEDTOOLTIP_BODY:String = "#ranked_battles:rankedBattlesCarousel/lockedTooltip/body";

        public static const RANKEDBATTLESBATTLERESULTS_ANIMATIONCHECKBOXLABEL:String = "#ranked_battles:rankedBattlesBattleResults/animationCheckBoxLabel";

        public static const RANKEDBATTLESVIEWHEADER_TABS_CURRENT:String = "#ranked_battles:rankedBattlesViewHeader/tabs/current";

        public static const RANKEDBATTLESVIEWHEADER_TABS_FINAL:String = "#ranked_battles:rankedBattlesViewHeader/tabs/final";

        public static const DIVISION_CLASSIFICATION:String = "#ranked_battles:division/classification";

        public static const DIVISION_BRONZE:String = "#ranked_battles:division/bronze";

        public static const DIVISION_SILVER:String = "#ranked_battles:division/silver";

        public static const DIVISION_GOLD:String = "#ranked_battles:division/gold";

        public static const DIVISION_STATUS_LOCKED:String = "#ranked_battles:division/status/locked";

        public static const DIVISION_STATUS_LOCKED_DESCRIPTION:String = "#ranked_battles:division/status/locked/description";

        public static const DIVISION_STATUS_COMPLETED:String = "#ranked_battles:division/status/completed";

        public static const DIVISION_STATUS_COMPLETED_DESCRIPTION:String = "#ranked_battles:division/status/completed/description";

        public static const DIVISION_STATUS_QUALIFICATION:String = "#ranked_battles:division/status/qualification";

        public static const DIVISION_STATUS_QUALIFICATION_PROGRESS:String = "#ranked_battles:division/status/qualification/progress";

        public static const REWARDSVIEW_TABS_RANKS:String = "#ranked_battles:rewardsView/tabs/ranks";

        public static const REWARDSVIEW_TABS_LEAGUES:String = "#ranked_battles:rewardsView/tabs/leagues";

        public static const REWARDSVIEW_TABS_LEAGUES_LEAGUE1:String = "#ranked_battles:rewardsView/tabs/leagues/league1";

        public static const REWARDSVIEW_TABS_LEAGUES_LEAGUE2:String = "#ranked_battles:rewardsView/tabs/leagues/league2";

        public static const REWARDSVIEW_TABS_LEAGUES_LEAGUE3:String = "#ranked_battles:rewardsView/tabs/leagues/league3";

        public static const REWARDSVIEW_TABS_LEAGUES_DESCRIPTION_LEAGUE1:String = "#ranked_battles:rewardsView/tabs/leagues/description/league1";

        public static const REWARDSVIEW_TABS_LEAGUES_DESCRIPTION_LEAGUE2:String = "#ranked_battles:rewardsView/tabs/leagues/description/league2";

        public static const REWARDSVIEW_TABS_LEAGUES_DESCRIPTION_LEAGUE3:String = "#ranked_battles:rewardsView/tabs/leagues/description/league3";

        public static const REWARDSVIEW_TABS_LEAGUES_AWARDDESCR:String = "#ranked_battles:rewardsView/tabs/leagues/awardDescr";

        public static const REWARDSVIEW_TABS_LEAGUES_DESCRIPTION:String = "#ranked_battles:rewardsView/tabs/leagues/description";

        public static const REWARDSVIEW_TABS_YEAR:String = "#ranked_battles:rewardsView/tabs/year";

        public static const REWARDSVIEW_TABS_YEAR_TITLE:String = "#ranked_battles:rewardsView/tabs/year/title";

        public static const REWARDSVIEW_TABS_YEAR_SCOREPOINT_TOOLTIP_HEADER:String = "#ranked_battles:rewardsView/tabs/year/scorePoint/tooltip/header";

        public static const REWARDSVIEW_TABS_YEAR_SCOREPOINT_TOOLTIP_BODY:String = "#ranked_battles:rewardsView/tabs/year/scorePoint/tooltip/body";

        public static const REWARDSVIEW_TABS_YEAR_CURRENT:String = "#ranked_battles:rewardsView/tabs/year/current";

        public static const REWARDSVIEW_COMMON_CURRENTREWARD:String = "#ranked_battles:rewardsView/common/currentReward";

        public static const DIVISION_TOOLTIP_RANKDESCRIPTION:String = "#ranked_battles:division/tooltip/rankDescription";

        public static const DIVISION_TOOLTIP_BATTLESDESCRIPTION:String = "#ranked_battles:division/tooltip/battlesDescription";

        public static const DIVISION_TOOLTIP_DESC_TITLE:String = "#ranked_battles:division/tooltip/desc/title";

        public static const DIVISION_TOOLTIP_DESC_CURRENT_TEXT:String = "#ranked_battles:division/tooltip/desc/current/text";

        public static const DIVISION_TOOLTIP_DESC_LOCKED_TEXT:String = "#ranked_battles:division/tooltip/desc/locked/text";

        public static const DIVISION_TOOLTIP_DESC_COMPLETED_TEXT:String = "#ranked_battles:division/tooltip/desc/completed/text";

        public static const DIVISION_TOOLTIP_DESC_CURRENTFINAL_TEXT:String = "#ranked_battles:division/tooltip/desc/currentFinal/text";

        public static const DIVISION_TOOLTIP_DESC_LOCKEDFINAL_TEXT:String = "#ranked_battles:division/tooltip/desc/lockedFinal/text";

        public static const DIVISION_TOOLTIP_DESC_COMPLETEDFINAL_TEXT:String = "#ranked_battles:division/tooltip/desc/completedFinal/text";

        public static const DIVISION_TOOLTIP_DESC_CURRENTQUAL_TEXT:String = "#ranked_battles:division/tooltip/desc/currentQual/text";

        public static const DIVISION_TOOLTIP_DESC_COMPLETEDQUAL_TEXT:String = "#ranked_battles:division/tooltip/desc/completedQual/text";

        public static const DIVISION_TOOLTIP_STATUS_CURRENT:String = "#ranked_battles:division/tooltip/status/current";

        public static const DIVISION_TOOLTIP_STATUS_CURRENTQUAL:String = "#ranked_battles:division/tooltip/status/currentQual";

        public static const DIVISION_TOOLTIP_STATUS_LOCKED:String = "#ranked_battles:division/tooltip/status/locked";

        public static const DIVISION_TOOLTIP_STATUS_LOCKED_DESC:String = "#ranked_battles:division/tooltip/status/locked/desc";

        public static const DIVISION_TOOLTIP_STATUS_LOCKED_DESCQUAL:String = "#ranked_battles:division/tooltip/status/locked/descQual";

        public static const DIVISION_TOOLTIP_STATUS_COMPLETED:String = "#ranked_battles:division/tooltip/status/completed";

        public static const DIVISION_TOOLTIP_STATUS_COMPLETEDQUAL:String = "#ranked_battles:division/tooltip/status/completedQual";

        public static const INTOPAGE_DESCRIPTION:String = "#ranked_battles:intoPage/description";

        public static const INTOPAGE_BTNS_ACCEPT:String = "#ranked_battles:intoPage/btns/accept";

        public static const INTOPAGE_BTNS_DETAILED:String = "#ranked_battles:intoPage/btns/detailed";

        public static const INTOPAGE_BLOCKS_BLOCK1_TITLE:String = "#ranked_battles:intoPage/blocks/block1/title";

        public static const INTOPAGE_BLOCKS_BLOCK1_DESCRIPTION:String = "#ranked_battles:intoPage/blocks/block1/description";

        public static const INTOPAGE_BLOCKS_BLOCK2_TITLE:String = "#ranked_battles:intoPage/blocks/block2/title";

        public static const INTOPAGE_BLOCKS_BLOCK2_DESCRIPTION:String = "#ranked_battles:intoPage/blocks/block2/description";

        public static const INTOPAGE_BLOCKS_BLOCK3_TITLE:String = "#ranked_battles:intoPage/blocks/block3/title";

        public static const INTOPAGE_BLOCKS_BLOCK3_DESCRIPTION:String = "#ranked_battles:intoPage/blocks/block3/description";

        public static const YEARREWARDS_TOOLTIP_BOX_SMALL_TITLE:String = "#ranked_battles:yearRewards/tooltip/box/small/title";

        public static const YEARREWARDS_TOOLTIP_BOX_MEDIUM_TITLE:String = "#ranked_battles:yearRewards/tooltip/box/medium/title";

        public static const YEARREWARDS_TOOLTIP_BOX_BIG_TITLE:String = "#ranked_battles:yearRewards/tooltip/box/big/title";

        public static const YEARREWARDS_TOOLTIP_BOX_LARGE_TITLE:String = "#ranked_battles:yearRewards/tooltip/box/large/title";

        public static const YEARREWARDS_TOOLTIP_BOX_SUBTITLE:String = "#ranked_battles:yearRewards/tooltip/box/subTitle";

        public static const YEARREWARDS_TOOLTIP_NEEDPOINTS:String = "#ranked_battles:yearRewards/tooltip/needPoints";

        public static const YEARREWARDS_TOOLTIP_ANY_DESCRIPTION_TITLE:String = "#ranked_battles:yearRewards/tooltip/any/description/title";

        public static const YEARREWARDS_TOOLTIP_SMALL_DESCRIPTION_NOTE_3:String = "#ranked_battles:yearRewards/tooltip/small/description/note/3";

        public static const YEARREWARDS_TOOLTIP_MEDIUM_DESCRIPTION_NOTE_3:String = "#ranked_battles:yearRewards/tooltip/medium/description/note/3";

        public static const YEARREWARDS_TOOLTIP_BIG_DESCRIPTION_NOTE_3:String = "#ranked_battles:yearRewards/tooltip/big/description/note/3";

        public static const YEARREWARDS_TOOLTIP_LARGE_DESCRIPTION_NOTE_3:String = "#ranked_battles:yearRewards/tooltip/large/description/note/3";

        public static const YEARREWARDS_TOOLTIP_SMALL_DESCRIPTION_NOTE_2:String = "#ranked_battles:yearRewards/tooltip/small/description/note/2";

        public static const YEARREWARDS_TOOLTIP_MEDIUM_DESCRIPTION_NOTE_2:String = "#ranked_battles:yearRewards/tooltip/medium/description/note/2";

        public static const YEARREWARDS_TOOLTIP_BIG_DESCRIPTION_NOTE_2:String = "#ranked_battles:yearRewards/tooltip/big/description/note/2";

        public static const YEARREWARDS_TOOLTIP_LARGE_DESCRIPTION_NOTE_2:String = "#ranked_battles:yearRewards/tooltip/large/description/note/2";

        public static const YEARREWARDS_TOOLTIP_REWARD_VEHICLE:String = "#ranked_battles:yearRewards/tooltip/reward/vehicle";

        public static const YEARREWARDS_TOOLTIP_STATUS_LOCKED_TITLE:String = "#ranked_battles:yearRewards/tooltip/status/locked/title";

        public static const YEARREWARDS_TOOLTIP_STATUS_LOCKED_DESCRIPTION:String = "#ranked_battles:yearRewards/tooltip/status/locked/description";

        public static const YEARREWARDS_TOOLTIP_STATUS_CURRENT_TITLE:String = "#ranked_battles:yearRewards/tooltip/status/current/title";

        public static const YEARREWARDS_TOOLTIP_STATUS_CURRENT_DESCRIPTION:String = "#ranked_battles:yearRewards/tooltip/status/current/description";

        public static const YEARREWARDS_TOOLTIP_STATUS_PASSED_TITLE:String = "#ranked_battles:yearRewards/tooltip/status/passed/title";

        public static const YEARREWARDS_TOOLTIP_STATUS_PASSED_DESCRIPTION:String = "#ranked_battles:yearRewards/tooltip/status/passed/description";

        public static const ALERTMESSAGE_SEASONFINISHED:String = "#ranked_battles:alertMessage/seasonFinished";

        public static const ALERTMESSAGE_SOMEPERIPHERIESHALT:String = "#ranked_battles:alertMessage/somePeripheriesHalt";

        public static const ALERTMESSAGE_SINGLEMODEHALT:String = "#ranked_battles:alertMessage/singleModeHalt";

        public static const ALERTMESSAGE_ALLPERIPHERIESHALT:String = "#ranked_battles:alertMessage/allPeripheriesHalt";

        public static const ALERTMESSAGE_BUTTON:String = "#ranked_battles:alertMessage/button";

        public static const QUALIFICATIONREWARDS_DESCRIPTION:String = "#ranked_battles:qualificationRewards/description";

        public function RANKED_BATTLES()
        {
            super();
        }
    }
}
