package
{
    public class BADGE extends Object
    {

        public static const ACCOUNTPOPOVER_BADGE_TOOLTIP:String = "#badge:accountPopover/badge/tooltip";

        public static const ACCOUNTPOPOVER_BADGE_TOOLTIP_BODY:String = "#badge:accountPopover/badge/tooltip/body";

        public static const ACCOUNTPOPOVER_BADGE_TOOLTIP_HEADER:String = "#badge:accountPopover/badge/tooltip/header";

        public static const BADGESPAGE_HEADER_DESCR:String = "#badge:badgesPage/header/descr";

        public static const BADGESPAGE_HEADER_BACKBTN_LABEL:String = "#badge:badgesPage/header/backBtn/label";

        public static const BADGESPAGE_HEADER_BACKBTN_DESCRLABEL:String = "#badge:badgesPage/header/backBtn/descrLabel";

        public static const BADGESPAGE_BODY_UNCOLLECTED_TITLE:String = "#badge:badgesPage/body/uncollected/title";

        public static const BADGESPAGE_DUMMY_TITLE:String = "#badge:badgesPage/dummy/title";

        public static const BADGESPAGE_DUMMY_DESCR:String = "#badge:badgesPage/dummy/descr";

        public static const BADGESPAGE_DUMMY_BUTTON_LABEL:String = "#badge:badgesPage/dummy/button/label";

        public static const BADGE_0:String = "#badge:badge_0";

        public static const BADGE_0_DESCR:String = "#badge:badge_0_descr";

        public static const BADGE_1:String = "#badge:badge_1";

        public static const BADGE_1_DESCR:String = "#badge:badge_1_descr";

        public static const BADGE_2:String = "#badge:badge_2";

        public static const BADGE_2_DESCR:String = "#badge:badge_2_descr";

        public static const BADGE_3:String = "#badge:badge_3";

        public static const BADGE_3_DESCR:String = "#badge:badge_3_descr";

        public static const BADGE_4:String = "#badge:badge_4";

        public static const BADGE_4_DESCR:String = "#badge:badge_4_descr";

        public static const BADGE_5:String = "#badge:badge_5";

        public static const BADGE_5_DESCR:String = "#badge:badge_5_descr";

        public static const BADGE_6:String = "#badge:badge_6";

        public static const BADGE_6_DESCR:String = "#badge:badge_6_descr";

        public static const BADGE_7:String = "#badge:badge_7";

        public static const BADGE_7_DESCR:String = "#badge:badge_7_descr";

        public static const BADGE_8:String = "#badge:badge_8";

        public static const BADGE_8_DESCR:String = "#badge:badge_8_descr";

        public static const BADGE_9:String = "#badge:badge_9";

        public static const BADGE_9_DESCR:String = "#badge:badge_9_descr";

        public static const BADGE_10:String = "#badge:badge_10";

        public static const BADGE_10_SHORT:String = "#badge:badge_10_short";

        public static const BADGE_10_DESCR:String = "#badge:badge_10_descr";

        public static const BADGE_11:String = "#badge:badge_11";

        public static const BADGE_11_SHORT:String = "#badge:badge_11_short";

        public static const BADGE_11_DESCR:String = "#badge:badge_11_descr";

        public static const BADGE_12:String = "#badge:badge_12";

        public static const BADGE_12_SHORT:String = "#badge:badge_12_short";

        public static const BADGE_12_DESCR:String = "#badge:badge_12_descr";

        public static const BADGE_13:String = "#badge:badge_13";

        public static const BADGE_13_SHORT:String = "#badge:badge_13_short";

        public static const BADGE_13_DESCR:String = "#badge:badge_13_descr";

        public static const BADGE_14:String = "#badge:badge_14";

        public static const BADGE_14_SHORT:String = "#badge:badge_14_short";

        public static const BADGE_14_DESCR:String = "#badge:badge_14_descr";

        public static const BADGE_15:String = "#badge:badge_15";

        public static const BADGE_15_SHORT:String = "#badge:badge_15_short";

        public static const BADGE_15_DESCR:String = "#badge:badge_15_descr";

        public static const BADGE_16:String = "#badge:badge_16";

        public static const BADGE_16_SHORT:String = "#badge:badge_16_short";

        public static const BADGE_16_DESCR:String = "#badge:badge_16_descr";

        public static const BADGE_17:String = "#badge:badge_17";

        public static const BADGE_17_SHORT:String = "#badge:badge_17_short";

        public static const BADGE_17_DESCR:String = "#badge:badge_17_descr";

        public static const BADGE_18:String = "#badge:badge_18";

        public static const BADGE_18_DESCR:String = "#badge:badge_18_descr";

        public static const BADGE_19:String = "#badge:badge_19";

        public static const BADGE_19_DESCR:String = "#badge:badge_19_descr";

        public static const BADGE_20:String = "#badge:badge_20";

        public static const BADGE_20_DESCR:String = "#badge:badge_20_descr";

        public static const BADGE_21:String = "#badge:badge_21";

        public static const BADGE_21_DESCR:String = "#badge:badge_21_descr";

        public static const BADGE_22:String = "#badge:badge_22";

        public static const BADGE_22_DESCR:String = "#badge:badge_22_descr";

        public static const BADGE_23:String = "#badge:badge_23";

        public static const BADGE_23_DESCR:String = "#badge:badge_23_descr";

        public static const BADGE_24:String = "#badge:badge_24";

        public static const BADGE_24_DESCR:String = "#badge:badge_24_descr";

        public static const BADGE_25:String = "#badge:badge_25";

        public static const BADGE_25_DESCR:String = "#badge:badge_25_descr";

        public static const BADGE_26:String = "#badge:badge_26";

        public static const BADGE_26_DESCR:String = "#badge:badge_26_descr";

        public static const BADGE_27:String = "#badge:badge_27";

        public static const BADGE_27_DESCR:String = "#badge:badge_27_descr";

        public static const BADGE_28:String = "#badge:badge_28";

        public static const BADGE_28_DESCR:String = "#badge:badge_28_descr";

        public static const BADGE_29:String = "#badge:badge_29";

        public static const BADGE_29_DESCR:String = "#badge:badge_29_descr";

        public static const BADGE_30:String = "#badge:badge_30";

        public static const BADGE_30_DESCR:String = "#badge:badge_30_descr";

        public static const BADGE_31:String = "#badge:badge_31";

        public static const BADGE_31_DESCR:String = "#badge:badge_31_descr";

        public static const BADGE_32:String = "#badge:badge_32";

        public static const BADGE_32_DESCR:String = "#badge:badge_32_descr";

        public static const BADGE_33:String = "#badge:badge_33";

        public static const BADGE_33_DESCR:String = "#badge:badge_33_descr";

        public static const BADGE_34:String = "#badge:badge_34";

        public static const BADGE_34_DESCR:String = "#badge:badge_34_descr";

        public static const BADGE_35:String = "#badge:badge_35";

        public static const BADGE_35_DESCR:String = "#badge:badge_35_descr";

        public static const BADGE_NOTE:String = "#badge:badge_note";

        public static const BADGE_36:String = "#badge:badge_36";

        public static const BADGE_36_SHORT:String = "#badge:badge_36_short";

        public static const BADGE_36_DESCR:String = "#badge:badge_36_descr";

        public static const BADGE_37:String = "#badge:badge_37";

        public static const BADGE_37_SHORT:String = "#badge:badge_37_short";

        public static const BADGE_37_DESCR:String = "#badge:badge_37_descr";

        public static const BADGE_38:String = "#badge:badge_38";

        public static const BADGE_38_SHORT:String = "#badge:badge_38_short";

        public static const BADGE_38_DESCR:String = "#badge:badge_38_descr";

        public static const BADGE_39:String = "#badge:badge_39";

        public static const BADGE_39_SHORT:String = "#badge:badge_39_short";

        public static const BADGE_39_DESCR:String = "#badge:badge_39_descr";

        public static const BADGE_40:String = "#badge:badge_40";

        public static const BADGE_40_SHORT:String = "#badge:badge_40_short";

        public static const BADGE_40_DESCR:String = "#badge:badge_40_descr";

        public static const BADGE_41:String = "#badge:badge_41";

        public static const BADGE_41_SHORT:String = "#badge:badge_41_short";

        public static const BADGE_41_DESCR:String = "#badge:badge_41_descr";

        public static const BADGE_42:String = "#badge:badge_42";

        public static const BADGE_42_DESCR:String = "#badge:badge_42_descr";

        public static const BADGE_43:String = "#badge:badge_43";

        public static const BADGE_43_DESCR:String = "#badge:badge_43_descr";

        public static const BADGE_44:String = "#badge:badge_44";

        public static const BADGE_44_DESCR:String = "#badge:badge_44_descr";

        public static const BADGE_45:String = "#badge:badge_45";

        public static const BADGE_45_DESCR:String = "#badge:badge_45_descr";

        public static const BADGE_46:String = "#badge:badge_46";

        public static const BADGE_46_DESCR:String = "#badge:badge_46_descr";

        public static const BADGE_47:String = "#badge:badge_47";

        public static const BADGE_47_DESCR:String = "#badge:badge_47_descr";

        public static const BADGE_48:String = "#badge:badge_48";

        public static const BADGE_48_DESCR:String = "#badge:badge_48_descr";

        public static const BADGE_49:String = "#badge:badge_49";

        public static const BADGE_49_DESCR:String = "#badge:badge_49_descr";

        public static const BADGE_50:String = "#badge:badge_50";

        public static const BADGE_50_DESCR:String = "#badge:badge_50_descr";

        public static const BADGE_51:String = "#badge:badge_51";

        public static const BADGE_51_DESCR:String = "#badge:badge_51_descr";

        public static const BADGE_52:String = "#badge:badge_52";

        public static const BADGE_52_DESCR:String = "#badge:badge_52_descr";

        public static const BADGE_56:String = "#badge:badge_56";

        public static const BADGE_56_SHORT:String = "#badge:badge_56_short";

        public static const BADGE_56_DESCR:String = "#badge:badge_56_descr";

        public static const BADGE_57:String = "#badge:badge_57";

        public static const BADGE_57_SHORT:String = "#badge:badge_57_short";

        public static const BADGE_57_DESCR:String = "#badge:badge_57_descr";

        public static const BADGE_58:String = "#badge:badge_58";

        public static const BADGE_58_SHORT:String = "#badge:badge_58_short";

        public static const BADGE_58_DESCR:String = "#badge:badge_58_descr";

        public static const BADGE_59:String = "#badge:badge_59";

        public static const BADGE_59_SHORT:String = "#badge:badge_59_short";

        public static const BADGE_59_DESCR:String = "#badge:badge_59_descr";

        public static const SUFFIX_BADGE_56:String = "#badge:suffix/badge_56";

        public static const SUFFIX_BADGE_57:String = "#badge:suffix/badge_57";

        public static const BADGE_60:String = "#badge:badge_60";

        public static const BADGE_60_DESCR:String = "#badge:badge_60_descr";

        public static const BADGE_61:String = "#badge:badge_61";

        public static const BADGE_61_DESCR:String = "#badge:badge_61_descr";

        public static const BADGE_62:String = "#badge:badge_62";

        public static const BADGE_62_DESCR:String = "#badge:badge_62_descr";

        public static const BADGE_63:String = "#badge:badge_63";

        public static const BADGE_63_DESCR:String = "#badge:badge_63_descr";

        public static const BADGE_64:String = "#badge:badge_64";

        public static const BADGE_64_DESCR:String = "#badge:badge_64_descr";

        public static const BADGE_65:String = "#badge:badge_65";

        public static const BADGE_65_DESCR:String = "#badge:badge_65_descr";

        public static const BADGE_66:String = "#badge:badge_66";

        public static const BADGE_66_DESCR:String = "#badge:badge_66_descr";

        public static const BADGE_67:String = "#badge:badge_67";

        public static const BADGE_67_DESCR:String = "#badge:badge_67_descr";

        public static const BADGE_68:String = "#badge:badge_68";

        public static const BADGE_68_DESCR:String = "#badge:badge_68_descr";

        public static const BADGE_69:String = "#badge:badge_69";

        public static const BADGE_69_DESCR:String = "#badge:badge_69_descr";

        public static const BADGE_70:String = "#badge:badge_70";

        public static const BADGE_70_DESCR:String = "#badge:badge_70_descr";

        public static const BADGE_71:String = "#badge:badge_71";

        public static const BADGE_71_DESCR:String = "#badge:badge_71_descr";

        public static const BADGE_72:String = "#badge:badge_72";

        public static const BADGE_72_DESCR:String = "#badge:badge_72_descr";

        public static const BADGE_73:String = "#badge:badge_73";

        public static const BADGE_73_DESCR:String = "#badge:badge_73_descr";

        public static const BADGE_74:String = "#badge:badge_74";

        public static const BADGE_74_DESCR:String = "#badge:badge_74_descr";

        public static const BADGE_75:String = "#badge:badge_75";

        public static const BADGE_75_DESCR:String = "#badge:badge_75_descr";

        public static const BADGE_76:String = "#badge:badge_76";

        public static const BADGE_76_DESCR:String = "#badge:badge_76_descr";

        public static const BADGE_77:String = "#badge:badge_77";

        public static const BADGE_77_DESCR:String = "#badge:badge_77_descr";

        public static const BADGE_78:String = "#badge:badge_78";

        public static const BADGE_78_DESCR:String = "#badge:badge_78_descr";

        public static const BADGE_79:String = "#badge:badge_79";

        public static const BADGE_79_DESCR:String = "#badge:badge_79_descr";

        public static const BADGE_80:String = "#badge:badge_80";

        public static const BADGE_80_DESCR:String = "#badge:badge_80_descr";

        public function BADGE()
        {
            super();
        }
    }
}
