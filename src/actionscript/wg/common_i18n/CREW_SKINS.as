package
{
    public class CREW_SKINS extends Object
    {

        public static const FEATURE_HEADER:String = "#crew_skins:feature/Header";

        public static const FEATURE_POSTER_POSTERHEADER:String = "#crew_skins:feature/poster/posterHeader";

        public static const FEATURE_POSTER_GIFTNAME:String = "#crew_skins:feature/poster/giftName";

        public static const FEATURE_POSTER_FEATUREDESCR:String = "#crew_skins:feature/poster/featureDescr";

        public static const FEATURE_POSTER_CONFIRM:String = "#crew_skins:feature/poster/confirm";

        public static const FEATURE_INUSE:String = "#crew_skins:feature/inUse";

        public static const FEATURE_INSTORAGE:String = "#crew_skins:feature/inStorage";

        public static const FEATURE_SKINSTABHEADER:String = "#crew_skins:feature/skinsTabHeader";

        public static const FEATURE_SOUND_HEADER:String = "#crew_skins:feature/sound/header";

        public static const FEATURE_TAB_RESTRICTIONS:String = "#crew_skins:feature/tab/restrictions";

        public static const FEATURE_TAB_ALLUSED:String = "#crew_skins:feature/tab/allUsed";

        public static const FEATURE_SOUND_NOSOUND:String = "#crew_skins:feature/sound/noSound";

        public static const FEATURE_NOCONTENT_HEADER:String = "#crew_skins:feature/noContent/header";

        public static const FEATURE_NOCONTENT_DESC1:String = "#crew_skins:feature/noContent/desc1";

        public static const FEATURE_NOCONTENT_DESC2:String = "#crew_skins:feature/noContent/desc2";

        public static const FEATURE_SKINUSEDWARNING:String = "#crew_skins:feature/skinUsedWarning";

        public static const RYBALKO_NAME:String = "#crew_skins:Rybalko/Name";

        public static const RYBALKO_LASTNAME:String = "#crew_skins:Rybalko/LastName";

        public static const RYBALKO_DESCR:String = "#crew_skins:Rybalko/Descr";

        public static const KATUKOV_NAME:String = "#crew_skins:Katukov/Name";

        public static const KATUKOV_LASTNAME:String = "#crew_skins:Katukov/LastName";

        public static const KATUKOV_DESCR:String = "#crew_skins:Katukov/Descr";

        public static const PATTON_NAME:String = "#crew_skins:Patton/Name";

        public static const PATTON_LASTNAME:String = "#crew_skins:Patton/LastName";

        public static const PATTON_DESCR:String = "#crew_skins:Patton/Descr";

        public static const DEGAULLE_NAME:String = "#crew_skins:DeGaulle/Name";

        public static const DEGAULLE_LASTNAME:String = "#crew_skins:DeGaulle/LastName";

        public static const DEGAULLE_DESCR:String = "#crew_skins:DeGaulle/Descr";

        public static const BRAVE01_NAME:String = "#crew_skins:Brave01/Name";

        public static const BRAVE01_LASTNAME:String = "#crew_skins:Brave01/LastName";

        public static const BRAVE01_DESCR:String = "#crew_skins:Brave01/Descr";

        public static const BRAVE02_NAME:String = "#crew_skins:Brave02/Name";

        public static const BRAVE02_LASTNAME:String = "#crew_skins:Brave02/LastName";

        public static const BRAVE02_DESCR:String = "#crew_skins:Brave02/Descr";

        public static const BRAVE03_NAME:String = "#crew_skins:Brave03/Name";

        public static const BRAVE03_LASTNAME:String = "#crew_skins:Brave03/LastName";

        public static const BRAVE03_DESCR:String = "#crew_skins:Brave03/Descr";

        public static const BRAVE04_NAME:String = "#crew_skins:Brave04/Name";

        public static const BRAVE04_LASTNAME:String = "#crew_skins:Brave04/LastName";

        public static const BRAVE04_DESCR:String = "#crew_skins:Brave04/Descr";

        public static const BRAVE05_NAME:String = "#crew_skins:Brave05/Name";

        public static const BRAVE05_LASTNAME:String = "#crew_skins:Brave05/LastName";

        public static const BRAVE05_DESCR:String = "#crew_skins:Brave05/Descr";

        public static const BRAVE06_NAME:String = "#crew_skins:Brave06/Name";

        public static const BRAVE06_LASTNAME:String = "#crew_skins:Brave06/LastName";

        public static const BRAVE06_DESCR:String = "#crew_skins:Brave06/Descr";

        public static const BRAVE07_NAME:String = "#crew_skins:Brave07/Name";

        public static const BRAVE07_LASTNAME:String = "#crew_skins:Brave07/LastName";

        public static const BRAVE07_DESCR:String = "#crew_skins:Brave07/Descr";

        public static const BRAVE08_NAME:String = "#crew_skins:Brave08/Name";

        public static const BRAVE08_LASTNAME:String = "#crew_skins:Brave08/LastName";

        public static const BRAVE08_DESCR:String = "#crew_skins:Brave08/Descr";

        public static const BRAVE09_NAME:String = "#crew_skins:Brave09/Name";

        public static const BRAVE09_LASTNAME:String = "#crew_skins:Brave09/LastName";

        public static const BRAVE09_DESCR:String = "#crew_skins:Brave09/Descr";

        public static const BRAVE10_NAME:String = "#crew_skins:Brave10/Name";

        public static const BRAVE10_LASTNAME:String = "#crew_skins:Brave10/LastName";

        public static const BRAVE10_DESCR:String = "#crew_skins:Brave10/Descr";

        public static const BRAVE11_NAME:String = "#crew_skins:Brave11/Name";

        public static const BRAVE11_LASTNAME:String = "#crew_skins:Brave11/LastName";

        public static const BRAVE11_DESCR:String = "#crew_skins:Brave11/Descr";

        public static const BRAVE12_NAME:String = "#crew_skins:Brave12/Name";

        public static const BRAVE12_LASTNAME:String = "#crew_skins:Brave12/LastName";

        public static const BRAVE12_DESCR:String = "#crew_skins:Brave12/Descr";

        public static const BRAVE13_NAME:String = "#crew_skins:Brave13/Name";

        public static const BRAVE13_LASTNAME:String = "#crew_skins:Brave13/LastName";

        public static const BRAVE13_DESCR:String = "#crew_skins:Brave13/Descr";

        public static const BRAVE14_NAME:String = "#crew_skins:Brave14/Name";

        public static const BRAVE14_LASTNAME:String = "#crew_skins:Brave14/LastName";

        public static const BRAVE14_DESCR:String = "#crew_skins:Brave14/Descr";

        public static const BRAVE15_NAME:String = "#crew_skins:Brave15/Name";

        public static const BRAVE15_LASTNAME:String = "#crew_skins:Brave15/LastName";

        public static const BRAVE15_DESCR:String = "#crew_skins:Brave15/Descr";

        public static const BRAVE16_NAME:String = "#crew_skins:Brave16/Name";

        public static const BRAVE16_LASTNAME:String = "#crew_skins:Brave16/LastName";

        public static const BRAVE16_DESCR:String = "#crew_skins:Brave16/Descr";

        public static const FOOLSDAY_CRAYFISH_NAME:String = "#crew_skins:FoolsDay_Crayfish/Name";

        public static const FOOLSDAY_CRAYFISH_LASTNAME:String = "#crew_skins:FoolsDay_Crayfish/LastName";

        public static const FOOLSDAY_CRAYFISH_DESCR:String = "#crew_skins:FoolsDay_Crayfish/Descr";

        public static const FOOLSDAY_DEER_NAME:String = "#crew_skins:FoolsDay_Deer/Name";

        public static const FOOLSDAY_DEER_LASTNAME:String = "#crew_skins:FoolsDay_Deer/LastName";

        public static const FOOLSDAY_DEER_DESCR:String = "#crew_skins:FoolsDay_Deer/Descr";

        public static const FOOLSDAY_TOMATO_NAME:String = "#crew_skins:FoolsDay_Tomato/Name";

        public static const FOOLSDAY_TOMATO_LASTNAME:String = "#crew_skins:FoolsDay_Tomato/LastName";

        public static const FOOLSDAY_TOMATO_DESCR:String = "#crew_skins:FoolsDay_Tomato/Descr";

        public static const FOOLSDAY_UNICORN_NAME:String = "#crew_skins:FoolsDay_Unicorn/Name";

        public static const FOOLSDAY_UNICORN_LASTNAME:String = "#crew_skins:FoolsDay_Unicorn/LastName";

        public static const FOOLSDAY_UNICORN_DESCR:String = "#crew_skins:FoolsDay_Unicorn/Descr";

        public static const FOOLSDAY_RNGESUS_NAME:String = "#crew_skins:FoolsDay_Rngesus/Name";

        public static const FOOLSDAY_RNGESUS_LASTNAME:String = "#crew_skins:FoolsDay_Rngesus/LastName";

        public static const FOOLSDAY_RNGESUS_DESCR:String = "#crew_skins:FoolsDay_Rngesus/Descr";

        public static const SPACE01_NAME:String = "#crew_skins:Space01/Name";

        public static const SPACE01_LASTNAME:String = "#crew_skins:Space01/LastName";

        public static const SPACE01_DESCR:String = "#crew_skins:Space01/Descr";

        public static const SPACE02_NAME:String = "#crew_skins:Space02/Name";

        public static const SPACE02_LASTNAME:String = "#crew_skins:Space02/LastName";

        public static const SPACE02_DESCR:String = "#crew_skins:Space02/Descr";

        public static const SPACE03_NAME:String = "#crew_skins:Space03/Name";

        public static const SPACE03_LASTNAME:String = "#crew_skins:Space03/LastName";

        public static const SPACE03_DESCR:String = "#crew_skins:Space03/Descr";

        public static const SPACE04_NAME:String = "#crew_skins:Space04/Name";

        public static const SPACE04_LASTNAME:String = "#crew_skins:Space04/LastName";

        public static const SPACE04_DESCR:String = "#crew_skins:Space04/Descr";

        public static const SPACE05_NAME:String = "#crew_skins:Space05/Name";

        public static const SPACE05_LASTNAME:String = "#crew_skins:Space05/LastName";

        public static const SPACE05_DESCR:String = "#crew_skins:Space05/Descr";

        public static const SPACE06_NAME:String = "#crew_skins:Space06/Name";

        public static const SPACE06_LASTNAME:String = "#crew_skins:Space06/LastName";

        public static const SPACE06_DESCR:String = "#crew_skins:Space06/Descr";

        public function CREW_SKINS()
        {
            super();
        }
    }
}
