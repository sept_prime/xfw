package
{
    public class INVITES extends Object
    {

        public static const INVITES_TEXT_EPIC:String = "#invites:invites/text/EPIC";

        public static const ERRORS_UNKNOWNUSER:String = "#invites:errors/unknownuser";

        public static const ERRORS_USEROFFLINE:String = "#invites:errors/useroffline";

        public static const ERRORS_SELFINVITE:String = "#invites:errors/selfinvite";

        public static const ERRORS_INVITENOTSUPPORTED:String = "#invites:errors/inviteNotSupported";

        public static const ERRORS_CREATIONNOTALLOWED:String = "#invites:errors/creationNotAllowed";

        public static const ERRORS_RECEIVERIGNORESENDER:String = "#invites:errors/receiverIgnoreSender";

        public static const ERRORS_VEHICLEBROKENORLOCKED:String = "#invites:errors/vehicleBrokenOrLocked";

        public static const ERRORS_INVALID:String = "#invites:errors/invalid";

        public static const INVITES_PREBATTLE_ACCEPTNOTALLOWED_UNDEFINEDPERIPHERY:String = "#invites:invites/prebattle/acceptNotAllowed/undefinedPeriphery";

        public static const INVITES_PREBATTLE_ACCEPTNOTALLOWED_OTHERPERIPHERY:String = "#invites:invites/prebattle/acceptNotAllowed/otherPeriphery";

        public static const INVITES_PREBATTLE_ALREADYJOINED_SQUAD:String = "#invites:invites/prebattle/alreadyJoined/SQUAD";

        public static const INVITES_PREBATTLE_ALREADYJOINED_FALLOUT:String = "#invites:invites/prebattle/alreadyJoined/FALLOUT";

        public static const INVITES_PREBATTLE_ALREADYJOINED_TRAINING:String = "#invites:invites/prebattle/alreadyJoined/TRAINING";

        public static const INVITES_PREBATTLE_ALREADYJOINED_EPIC_TRAINING:String = "#invites:invites/prebattle/alreadyJoined/EPIC_TRAINING";

        public static const INVITES_PREBATTLE_ALREADYJOINED_CLAN:String = "#invites:invites/prebattle/alreadyJoined/CLAN";

        public static const INVITES_PREBATTLE_ALREADYJOINED_TOURNAMENT:String = "#invites:invites/prebattle/alreadyJoined/TOURNAMENT";

        public static const INVITES_PREBATTLE_ALREADYJOINED_UNIT:String = "#invites:invites/prebattle/alreadyJoined/UNIT";

        public static const INVITES_PREBATTLE_ALREADYJOINED_SORTIE:String = "#invites:invites/prebattle/alreadyJoined/SORTIE";

        public static const INVITES_TEXT_CREATORNAME:String = "#invites:invites/text/creatorName";

        public static const INVITES_TEXT_SQUAD:String = "#invites:invites/text/SQUAD";

        public static const INVITES_TEXT_EVENT:String = "#invites:invites/text/EVENT";

        public static const INVITES_TEXT_FALLOUT:String = "#invites:invites/text/FALLOUT";

        public static const INVITES_TEXT_FALLOUT_FALLOUT_CLASSIC:String = "#invites:invites/text/FALLOUT/FALLOUT_CLASSIC";

        public static const INVITES_TEXT_FALLOUT_FALLOUT_MULTITEAM:String = "#invites:invites/text/FALLOUT/FALLOUT_MULTITEAM";

        public static const INVITES_TEXT_TRAINING:String = "#invites:invites/text/TRAINING";

        public static const INVITES_TEXT_EPIC_TRAINING:String = "#invites:invites/text/EPIC_TRAINING";

        public static const INVITES_TEXT_UNIT:String = "#invites:invites/text/UNIT";

        public static const INVITES_TEXT_SORTIE:String = "#invites:invites/text/SORTIE";

        public static const INVITES_TEXT_FORT_BATTLE:String = "#invites:invites/text/FORT_BATTLE";

        public static const INVITES_TEXT_EXTERNAL:String = "#invites:invites/text/EXTERNAL";

        public static const INVITES_TEXT_FORT_OFFENCE:String = "#invites:invites/text/fort/offence";

        public static const INVITES_TEXT_FORT_DEFENCE:String = "#invites:invites/text/fort/defence";

        public static const INVITES_TEXT_FORT_DIRECTION:String = "#invites:invites/text/fort/direction";

        public static const CLAN_APPLICATIONS_TITLE:String = "#invites:clan/applications/title";

        public static const CLAN_APPLICATIONS_COMMENT:String = "#invites:clan/applications/comment";

        public static const CLAN_APPLICATIONS_COMMENT_MINIMAP:String = "#invites:clan/applications/comment/minimap";

        public static const CLAN_APPLICATIONS_BUTTONS_DETAILS:String = "#invites:clan/applications/buttons/details";

        public static const CLAN_PERSONAL_INVITES_TITLE:String = "#invites:clan/personal/invites/title";

        public static const CLAN_PERSONAL_INVITES_BUTTONS_DETAILS:String = "#invites:clan/personal/invites/buttons/details";

        public static const CLAN_APPLICATION_TITLE:String = "#invites:clan/application/title";

        public static const CLAN_APPLICATION_COMMENT:String = "#invites:clan/application/comment";

        public static const CLAN_INVITE_TITLE:String = "#invites:clan/invite/title";

        public static const CLAN_INVITE_COMMENT:String = "#invites:clan/invite/comment";

        public static const INVITES_COMMENT:String = "#invites:invites/comment";

        public static const INVITES_STATE_PENDING:String = "#invites:invites/state/PENDING";

        public static const INVITES_STATE_ACCEPTED:String = "#invites:invites/state/ACCEPTED";

        public static const INVITES_STATE_DECLINED:String = "#invites:invites/state/DECLINED";

        public static const INVITES_STATE_REVOKED:String = "#invites:invites/state/REVOKED";

        public static const INVITES_STATE_EXPIRED:String = "#invites:invites/state/EXPIRED";

        public static const INVITES_STATE_ERROR:String = "#invites:invites/state/ERROR";

        public static const INVITES_NOTE_SERVER_CHANGE:String = "#invites:invites/note/server_change";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_EVENT:String = "#invites:invites/note/change_and_leave/EVENT";

        public static const INVITES_NOTE_LEAVE_SQUAD:String = "#invites:invites/note/leave/SQUAD";

        public static const INVITES_NOTE_LEAVE_FALLOUT:String = "#invites:invites/note/leave/FALLOUT";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_SQUAD:String = "#invites:invites/note/change_and_leave/SQUAD";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_FALLOUT:String = "#invites:invites/note/change_and_leave/FALLOUT";

        public static const INVITES_NOTE_LEAVE_TRAINING:String = "#invites:invites/note/leave/TRAINING";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_TRAINING:String = "#invites:invites/note/change_and_leave/TRAINING";

        public static const INVITES_NOTE_LEAVE_EPIC_TRAINING:String = "#invites:invites/note/leave/EPIC_TRAINING";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_EPIC_TRAINING:String = "#invites:invites/note/change_and_leave/EPIC_TRAINING";

        public static const INVITES_NOTE_LEAVE_UNIT:String = "#invites:invites/note/leave/UNIT";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_UNIT:String = "#invites:invites/note/change_and_leave/UNIT";

        public static const INVITES_NOTE_LEAVE_CLAN:String = "#invites:invites/note/leave/CLAN";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_CLAN:String = "#invites:invites/note/change_and_leave/CLAN";

        public static const INVITES_NOTE_LEAVE_TOURNAMENT:String = "#invites:invites/note/leave/TOURNAMENT";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_TOURNAMENT:String = "#invites:invites/note/change_and_leave/TOURNAMENT";

        public static const INVITES_NOTE_LEAVE_SORTIE:String = "#invites:invites/note/leave/SORTIE";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_SORTIE:String = "#invites:invites/note/change_and_leave/SORTIE";

        public static const INVITES_NOTE_LEAVE_FORT_COMMON:String = "#invites:invites/note/leave/FORT_COMMON";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_FORT_COMMON:String = "#invites:invites/note/change_and_leave/FORT_COMMON";

        public static const INVITES_NOTE_LEAVE_E_SPORT_COMMON:String = "#invites:invites/note/leave/E_SPORT_COMMON";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_E_SPORT_COMMON:String = "#invites:invites/note/change_and_leave/E_SPORT_COMMON";

        public static const INVITES_NOTE_LEAVE_FORT_BATTLE:String = "#invites:invites/note/leave/FORT_BATTLE";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_FORT_BATTLE:String = "#invites:invites/note/change_and_leave/FORT_BATTLE";

        public static const INVITES_NOTE_LEAVE_EXTERNAL:String = "#invites:invites/note/leave/EXTERNAL";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_EXTERNAL:String = "#invites:invites/note/change_and_leave/EXTERNAL";

        public static const INVITES_NOTE_LEAVE_RANDOMS:String = "#invites:invites/note/leave/RANDOMS";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_RANDOMS:String = "#invites:invites/note/change_and_leave/RANDOMS";

        public static const INVITES_NOTE_LEAVE_EVENT:String = "#invites:invites/note/leave/EVENT";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_EVENT_BATTLES:String = "#invites:invites/note/change_and_leave/EVENT_BATTLES";

        public static const INVITES_NOTE_LEAVE_SANDBOX:String = "#invites:invites/note/leave/SANDBOX";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_SANDBOX:String = "#invites:invites/note/change_and_leave/SANDBOX";

        public static const INVITES_NOTE_LEAVE_EPIC:String = "#invites:invites/note/leave/EPIC";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_EPIC:String = "#invites:invites/note/change_and_leave/EPIC";

        public static const INVITES_NOTE_LEAVE_RANKED:String = "#invites:invites/note/leave/RANKED";

        public static const INVITES_NOTE_CHANGE_AND_LEAVE_RANKED:String = "#invites:invites/note/change_and_leave/RANKED";

        public static const GUI_TITLES_RECEIVEDINVITES:String = "#invites:gui/titles/receivedInvites";

        public static const GUI_TITLES_INVITE:String = "#invites:gui/titles/invite";

        public static const GUI_TITLES_BARTER:String = "#invites:gui/titles/barter";

        public static const GUI_TITLES_CLAN:String = "#invites:gui/titles/clan";

        public static const GUI_LABELS_RECEIVER:String = "#invites:gui/labels/receiver";

        public static const GUI_LABELS_INVITETEXT:String = "#invites:gui/labels/inviteText";

        public static const GUI_LABELS_ADDITIONALTEXT:String = "#invites:gui/labels/additionalText";

        public static const GUI_BUTTONS_SEND:String = "#invites:gui/buttons/send";

        public static const GUI_BUTTONS_ACCEPT:String = "#invites:gui/buttons/accept";

        public static const GUI_BUTTONS_REJECT:String = "#invites:gui/buttons/reject";

        public static const GUI_BUTTONS_CANCEL:String = "#invites:gui/buttons/cancel";

        public static const FRIENDSHIP_REQUEST_TEXT:String = "#invites:friendship/request/text";

        public static const FRIENDSHIP_NOTE_APPROVED:String = "#invites:friendship/note/approved";

        public static const FRIENDSHIP_NOTE_CANCELED:String = "#invites:friendship/note/canceled";

        public static const FRIENDSHIP_NOTE_PROCESS:String = "#invites:friendship/note/process";

        public static const FRIENDSHIP_NOTE_MAXROSTER:String = "#invites:friendship/note/maxRoster";

        public static const FRIENDSHIP_NOTE_NOTCONNECTED:String = "#invites:friendship/note/notConnected";

        public static const CLANS_STATE_APP_ACTIVE:String = "#invites:clans/state/app/active";

        public static const CLANS_STATE_APP_ACCEPTED:String = "#invites:clans/state/app/accepted";

        public static const CLANS_STATE_APP_DECLINED:String = "#invites:clans/state/app/declined";

        public static const CLANS_STATE_APP_ERROR_INCLANENTERCOOLDOWN:String = "#invites:clans/state/app/error/inClanEnterCooldown";

        public static const CLANS_STATE_INVITE_ACTIVE:String = "#invites:clans/state/invite/active";

        public static const CLANS_STATE_INVITE_ACCEPTED:String = "#invites:clans/state/invite/accepted";

        public static const CLANS_STATE_INVITE_DECLINED:String = "#invites:clans/state/invite/declined";

        public static const CLANS_STATE_INVITE_ERROR_INCLANENTERCOOLDOWN:String = "#invites:clans/state/invite/error/inClanEnterCooldown";

        public static const STRONGHOLD_INVITE_SENDINVITETOUSERNAME:String = "#invites:stronghold/invite/sendInviteToUsername";

        public function INVITES()
        {
            super();
        }
    }
}
