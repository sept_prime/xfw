#!/bin/bash

# XVM team (c) https://modxvm.com 2014-2019
# XFW Framework build system

set -e

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_actionscript_sdk

class="\$AppLinks"
build_as3_swc \
    -source-path wg/battle_ui/classic \
    -source-path wg/battle_ui/ui/* \
    -source-path wg/battle \
    -source-path wg/common_i18n \
    -external-library-path+=../../~output/swc/wg_battle.swc \
    -output ../../~output/swc/wg_battle_classic_ui.swc \
    -include-classes $class
