### XFW framework ###
##### Source folder #####

## Content ##

 |folder       | description                                |
 |--------------------|-------------------------------------|
 |actionscript | wg.swc, xfw.swc and xfw.swf sources        |
 |swf          | patches for lobby.swf and battle.swf files |
 |python       | python part of XWF                         |
