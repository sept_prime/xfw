#!/bin/bash

# XVM team (c) https://modxvm.com 2013-2019
# XFW Framework build system

EXTRACT_ONLY=1

files=(
        'battle'
        'battleVehicleMarkersApp'
        'lobby'
      )

###

currentdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$currentdir"/../../build/library.sh

detect_os
detect_arch

PATH="$currentdir"/../../build/bin/"$OS"_"$arch"/:$PATH

detect_patch

if [ "$XVMBUILD_XFWROOT_PATH" == "" ]; then
  XVMBUILD_XFWROOT_PATH="$currentdir/../.."
fi

if [ "$XVMBUILD_XFW_WG_OUTPUTPATH" == "" ]; then
  XVMBUILD_XFW_WG_OUTPUTPATH="~output/swf_wg"
fi

for (( i=0; i<${#files[@]}; i++ )); do
    name=${files[$i]}
    rm -rf $name.orig-0 $name.orig-0.abc $name.orig.swf
    cp -f flash/$name.swf .
done

for file in *.swf
do
  mv "$file" "${file%.swf}.orig.swf"
done

for (( i=0; i<${#files[@]}; i++ )); do
    name=${files[$i]}
    echo "patching $name.swf"

    abcexport $name.orig.swf
    rabcdasm $name.orig-0.abc

    [ "$EXTRACT_ONLY" = "1" ] && continue

    for patch_filename in $name.*.patch; do
        echo "=> $patch_filename"
        patch --binary -p0 < $patch_filename || exit 1
    done

    rabcasm $name.orig-0/$name.orig-0.main.asasm
    abcreplace $name.orig.swf 0 $name.orig-0/$name.orig-0.main.abc
    rm $name.orig-0.abc
    rm -rf $name.orig-0

    mkdir -p "$XVMBUILD_XFWROOT_PATH"/"$XVMBUILD_XFW_WG_OUTPUTPATH"

    #TODO: disabled, because of the strange behavior on WoT 1.1
    #mv $name.orig.swf "$XVMBUILD_XFWROOT_PATH"/"$XVMBUILD_XFW_WG_OUTPUTPATH"/xfw_$name.swf
    mv $name.orig.swf "$XVMBUILD_XFWROOT_PATH"/"$XVMBUILD_XFW_WG_OUTPUTPATH"/$name.swf
done

echo ""
